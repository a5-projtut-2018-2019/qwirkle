﻿Imports System.Collections.Generic
Imports QwirkleEngine

Public Class frm_PlateauJeu

    Dim partie As Partie
    Dim dernierClique As PBCase

    Private Sub RendreGrille(ByVal grille As Grille)
        Dim tableauCases(,) As [Case]
        tableauCases = grille.GetCases()

        tlp_grille.Controls.Clear()
        tlp_grille.ColumnStyles.Clear()
        tlp_grille.RowStyles.Clear()

        tlp_grille.ColumnCount = tableauCases.GetLength(0) + 1
        tlp_grille.RowCount = tableauCases.GetLength(1) + 1

        For colonne As Integer = 0 To tlp_grille.ColumnCount - 1
            tlp_grille.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, VariablesGlobales.coteTuile))
        Next
        For ligne As Integer = 0 To tlp_grille.RowCount - 1
            tlp_grille.RowStyles.Add(New RowStyle(SizeType.Absolute, VariablesGlobales.coteTuile))
        Next


        Dim largeur, hauteur As Integer
        For largeur = 0 To tableauCases.GetLength(0) - 1
            For hauteur = 0 To tableauCases.GetLength(1) - 1
                Dim caseAffichage As New PBCase(tableauCases(largeur, hauteur).GetTuile(), largeur, tableauCases.GetLength(1) - 1 - hauteur)
                tlp_grille.Controls.Add(caseAffichage, largeur, hauteur)
                AddHandler caseAffichage.Click, AddressOf Me.PBCase_Grille_Click
            Next
        Next
    End Sub

    Public Sub Rendre6Tuiles(ByRef tlp_destination As TableLayoutPanel, ByVal tuiles As List(Of Tuile))
        tlp_destination.Controls.Clear()
        Dim tlpIndex As Integer
        For tlpIndex = 0 To tuiles.Count - 1
            Dim couleur As Tuile.Couleur = tuiles(tlpIndex).GetCouleur()
            Dim forme As Tuile.Forme = tuiles(tlpIndex).GetForme()
            Dim caseAffichage As New PBCase(tuiles(tlpIndex), 0, 0)
            caseAffichage.BackColor = Color.Transparent
            AddHandler caseAffichage.Click, AddressOf Me.PBCase_Reserve_Click
            tlp_destination.Controls.Add(caseAffichage, tlpIndex, 0)
        Next
    End Sub

    Private Sub Frm_PlateauJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim listeJoueurs As New List(Of Joueur)
        Select Case VariablesGlobales.nbrJoueurs
            Case 2
                tlp_joueur3.Enabled = False
                lbl_score_j3.Text = "0"
                tlp_joueur4.Enabled = False
                lbl_score_j4.Text = "0"

                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(0)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(1)))
            Case 3
                tlp_joueur4.Enabled = False
                lbl_score_j4.Text = "0"

                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(0)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(1)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(2)))
            Case 4
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(0)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(1)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(2)))
                listeJoueurs.Add(New Joueur(VariablesGlobales.nomsJoueurs(3)))
        End Select

        partie = New Partie(listeJoueurs)
        listeJoueurs = partie.GetListeJoueursOrdonnee()

        Dim indexJoueur As Integer
        For indexJoueur = 0 To listeJoueurs.Count - 1
            tlp_plateau_jeu.Controls("grb_scores").Controls("tlp_all_joueur").Controls("tlp_joueur" & indexJoueur + 1).Controls("lbl_nom_j" & indexJoueur + 1).Text = listeJoueurs(indexJoueur).GetNom()
            tlp_plateau_jeu.Controls("grb_scores").Controls("tlp_all_joueur").Controls("tlp_joueur" & indexJoueur + 1).Controls("lbl_score_j" & indexJoueur + 1).Text = "0"
        Next

        RendreGrille(partie.GetGrille())
        Rendre6Tuiles(Me.tlp_stocktuiles, partie.GetTourActuel().GetJoueur().GetTuiles())

        Dim indexJoueurActuel As Integer = partie.GetIndexJoueurActuel()
        tlp_all_joueur.Controls("tlp_joueur" & indexJoueurActuel + 1).Controls("lbl_nom_j" & indexJoueurActuel + 1).ForeColor = Color.Green

        cmd_echange.Enabled = False
        cmd_valider.Enabled = False
    End Sub

    Private Sub validerTour()
        Dim indexJoueurQuiValide As Integer = partie.GetIndexJoueurActuel()
        partie.ValiderTour() : RendreGrille(partie.GetGrillePartieEtTour()) : Rendre6Tuiles(tlp_stocktuiles, partie.GetTourActuel().GetJoueur().GetTuiles())
        Dim indexJoueurSuivant As Integer = partie.GetIndexJoueurActuel()
        tlp_all_joueur.Controls("tlp_joueur" & indexJoueurQuiValide + 1).Controls("lbl_score_j" & indexJoueurQuiValide + 1).Text = partie.GetListeJoueursOrdonnee()(indexJoueurQuiValide).GetPoints()
        tlp_all_joueur.Controls("tlp_joueur" & indexJoueurQuiValide + 1).Controls("lbl_nom_j" & indexJoueurQuiValide + 1).ForeColor = SystemColors.ControlText
        tlp_all_joueur.Controls("tlp_joueur" & indexJoueurSuivant + 1).Controls("lbl_nom_j" & indexJoueurSuivant + 1).ForeColor = Color.Green

        If partie.EstTerminee() Then
            frm_ResumerFinPartie.Show()
            VariablesGlobales.partieQuandElleEstFinie = partie
            Me.Close()
        End If

        cmd_echange.Enabled = True
        cmd_valider.Enabled = False
    End Sub

    Private Sub cmd_valider_Click(sender As Object, e As EventArgs) Handles cmd_valider.Click
        validerTour()
    End Sub

    Private Sub cmd_echange_Click(sender As Object, e As EventArgs) Handles cmd_echange.Click
        VariablesGlobales.tuilesJoueurEchange = partie.GetTourActuel().GetJoueur().GetTuiles()
        frm_Echanger.ShowDialog()
        If VariablesGlobales.casesCliqueesEchange.Count > 0 Then
            Dim tuilesEchange As New List(Of Tuile)
            For Each caseEchange As PBCase In VariablesGlobales.casesCliqueesEchange
                tuilesEchange.Add(caseEchange.GetTuile())
            Next
            partie.GetTourActuel().EchangerTuiles(tuilesEchange)
            validerTour()
        End If
        VariablesGlobales.casesCliqueesEchange.Clear()
    End Sub

    Private Sub cmd_annuler_Click(sender As Object, e As EventArgs) Handles cmd_annuler.Click
        partie.GetTourActuel().AnnulerPlacements()
        Rendre6Tuiles(tlp_stocktuiles, partie.GetTourActuel().GetJoueur().GetTuiles())
        RendreGrille(partie.GetGrillePartieEtTour())

        If partie.GetHistoriqueTours().Count > 0 Then
            cmd_echange.Enabled = True
        End If
        cmd_valider.Enabled = False
    End Sub

    Private Sub PBCase_Reserve_Click(sender As PBCase, e As EventArgs)
        If dernierClique Is Nothing Then
            sender.ToggleEstClique()
            dernierClique = sender
        ElseIf dernierClique.GetEstClique() = True Then
            dernierClique.ToggleEstClique()
            sender.ToggleEstClique()
            dernierClique = sender
        ElseIf dernierClique.GetEstClique() = False Then
            sender.ToggleEstClique()
            dernierClique = sender
        End If

        Dim x As Integer
        Dim y As Integer
        For x = 0 To tlp_grille.ColumnCount - 2
            For y = 0 To tlp_grille.RowCount - 2
                Dim caseAffichee As PBCase = tlp_grille.GetControlFromPosition(x, y)
                caseAffichee.EstValide(False)
            Next
        Next

        Dim casesPossibles As New List(Of [Case])
        casesPossibles = partie.CasesPossibles(dernierClique.GetTuile())
        For Each casePossible As [Case] In casesPossibles
            Dim caseAffichee As PBCase = tlp_grille.GetControlFromPosition(casePossible.GetX(), casePossible.GetY())
            caseAffichee.EstValide(True)
        Next
    End Sub

    Private Sub PBCase_Grille_Click(sender As PBCase, e As EventArgs)
        If dernierClique IsNot Nothing And sender.GetTuile() Is Nothing Then
            Dim placementOK As Boolean = partie.VerifierPlacerTuile(dernierClique.GetTuile(), sender.GetX(), tlp_grille.RowCount - 2 - sender.GetY())
            If placementOK Then
                partie.GetTourActuel().PlacerTuile(dernierClique.GetTuile(), sender.GetX(), tlp_grille.RowCount - 2 - sender.GetY())
                RendreGrille(partie.GetGrillePartieEtTour())
                Rendre6Tuiles(tlp_stocktuiles, partie.GetTourActuel().GetTuilesJoueur())
                dernierClique = Nothing
                cmd_echange.Enabled = False
                cmd_valider.Enabled = True
            Else
                MsgBox("Placement non autorisé", vbOKOnly + vbCritical, "Erreur")
            End If
        End If
    End Sub
End Class