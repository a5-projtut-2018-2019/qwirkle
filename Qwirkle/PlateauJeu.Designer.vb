﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_PlateauJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_PlateauJeu))
        Me.lbl_score_j1 = New System.Windows.Forms.Label()
        Me.lbl_nom_j1 = New System.Windows.Forms.Label()
        Me.grb_scores = New System.Windows.Forms.GroupBox()
        Me.tlp_all_joueur = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j2 = New System.Windows.Forms.Label()
        Me.lbl_score_j2 = New System.Windows.Forms.Label()
        Me.tlp_joueur4 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j4 = New System.Windows.Forms.Label()
        Me.lbl_score_j4 = New System.Windows.Forms.Label()
        Me.tlp_joueur3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j3 = New System.Windows.Forms.Label()
        Me.lbl_score_j3 = New System.Windows.Forms.Label()
        Me.tlp_plateau_jeu = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_commandes = New System.Windows.Forms.TableLayoutPanel()
        Me.cmd_echange = New System.Windows.Forms.Button()
        Me.cmd_valider = New System.Windows.Forms.Button()
        Me.cmd_annuler = New System.Windows.Forms.Button()
        Me.tlp_stocktuiles = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_grille = New System.Windows.Forms.TableLayoutPanel()
        Me.grb_scores.SuspendLayout()
        Me.tlp_all_joueur.SuspendLayout()
        Me.tlp_joueur1.SuspendLayout()
        Me.tlp_joueur2.SuspendLayout()
        Me.tlp_joueur4.SuspendLayout()
        Me.tlp_joueur3.SuspendLayout()
        Me.tlp_plateau_jeu.SuspendLayout()
        Me.tlp_commandes.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_score_j1
        '
        Me.lbl_score_j1.AutoSize = True
        Me.lbl_score_j1.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j1.Location = New System.Drawing.Point(108, 0)
        Me.lbl_score_j1.Name = "lbl_score_j1"
        Me.lbl_score_j1.Size = New System.Drawing.Size(16, 26)
        Me.lbl_score_j1.TabIndex = 1
        Me.lbl_score_j1.Text = "1"
        Me.lbl_score_j1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_nom_j1
        '
        Me.lbl_nom_j1.AutoSize = True
        Me.lbl_nom_j1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j1.Location = New System.Drawing.Point(38, 0)
        Me.lbl_nom_j1.Name = "lbl_nom_j1"
        Me.lbl_nom_j1.Size = New System.Drawing.Size(64, 26)
        Me.lbl_nom_j1.TabIndex = 0
        Me.lbl_nom_j1.Text = "Joueur 1"
        Me.lbl_nom_j1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grb_scores
        '
        Me.grb_scores.Controls.Add(Me.tlp_all_joueur)
        Me.grb_scores.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grb_scores.Location = New System.Drawing.Point(3, 2)
        Me.grb_scores.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.grb_scores.MaximumSize = New System.Drawing.Size(9999999, 50)
        Me.grb_scores.Name = "grb_scores"
        Me.grb_scores.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.grb_scores.Size = New System.Drawing.Size(876, 49)
        Me.grb_scores.TabIndex = 2
        Me.grb_scores.TabStop = False
        Me.grb_scores.Text = "Score des joueurs"
        '
        'tlp_all_joueur
        '
        Me.tlp_all_joueur.ColumnCount = 4
        Me.tlp_all_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp_all_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp_all_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp_all_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp_all_joueur.Controls.Add(Me.tlp_joueur1, 0, 0)
        Me.tlp_all_joueur.Controls.Add(Me.tlp_joueur2, 1, 0)
        Me.tlp_all_joueur.Controls.Add(Me.tlp_joueur4, 3, 0)
        Me.tlp_all_joueur.Controls.Add(Me.tlp_joueur3, 2, 0)
        Me.tlp_all_joueur.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_all_joueur.Location = New System.Drawing.Point(3, 17)
        Me.tlp_all_joueur.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_all_joueur.Name = "tlp_all_joueur"
        Me.tlp_all_joueur.RowCount = 1
        Me.tlp_all_joueur.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_all_joueur.Size = New System.Drawing.Size(870, 30)
        Me.tlp_all_joueur.TabIndex = 5
        '
        'tlp_joueur1
        '
        Me.tlp_joueur1.ColumnCount = 2
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Controls.Add(Me.lbl_nom_j1, 0, 0)
        Me.tlp_joueur1.Controls.Add(Me.lbl_score_j1, 1, 0)
        Me.tlp_joueur1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur1.Location = New System.Drawing.Point(3, 2)
        Me.tlp_joueur1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur1.Name = "tlp_joueur1"
        Me.tlp_joueur1.RowCount = 1
        Me.tlp_joueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Size = New System.Drawing.Size(211, 26)
        Me.tlp_joueur1.TabIndex = 2
        '
        'tlp_joueur2
        '
        Me.tlp_joueur2.ColumnCount = 2
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Controls.Add(Me.lbl_nom_j2, 0, 0)
        Me.tlp_joueur2.Controls.Add(Me.lbl_score_j2, 1, 0)
        Me.tlp_joueur2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur2.Location = New System.Drawing.Point(220, 2)
        Me.tlp_joueur2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur2.Name = "tlp_joueur2"
        Me.tlp_joueur2.RowCount = 1
        Me.tlp_joueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Size = New System.Drawing.Size(211, 26)
        Me.tlp_joueur2.TabIndex = 3
        '
        'lbl_nom_j2
        '
        Me.lbl_nom_j2.AutoSize = True
        Me.lbl_nom_j2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j2.Location = New System.Drawing.Point(38, 0)
        Me.lbl_nom_j2.Name = "lbl_nom_j2"
        Me.lbl_nom_j2.Size = New System.Drawing.Size(64, 26)
        Me.lbl_nom_j2.TabIndex = 0
        Me.lbl_nom_j2.Text = "Joueur 2"
        Me.lbl_nom_j2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j2
        '
        Me.lbl_score_j2.AutoSize = True
        Me.lbl_score_j2.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j2.Location = New System.Drawing.Point(108, 0)
        Me.lbl_score_j2.Name = "lbl_score_j2"
        Me.lbl_score_j2.Size = New System.Drawing.Size(16, 26)
        Me.lbl_score_j2.TabIndex = 1
        Me.lbl_score_j2.Text = "2"
        Me.lbl_score_j2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur4
        '
        Me.tlp_joueur4.ColumnCount = 2
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Controls.Add(Me.lbl_nom_j4, 0, 0)
        Me.tlp_joueur4.Controls.Add(Me.lbl_score_j4, 1, 0)
        Me.tlp_joueur4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur4.Location = New System.Drawing.Point(654, 2)
        Me.tlp_joueur4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur4.Name = "tlp_joueur4"
        Me.tlp_joueur4.RowCount = 1
        Me.tlp_joueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Size = New System.Drawing.Size(213, 26)
        Me.tlp_joueur4.TabIndex = 4
        '
        'lbl_nom_j4
        '
        Me.lbl_nom_j4.AutoSize = True
        Me.lbl_nom_j4.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j4.Location = New System.Drawing.Point(39, 0)
        Me.lbl_nom_j4.Name = "lbl_nom_j4"
        Me.lbl_nom_j4.Size = New System.Drawing.Size(64, 26)
        Me.lbl_nom_j4.TabIndex = 0
        Me.lbl_nom_j4.Text = "Joueur 4"
        Me.lbl_nom_j4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j4
        '
        Me.lbl_score_j4.AutoSize = True
        Me.lbl_score_j4.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j4.Location = New System.Drawing.Point(109, 0)
        Me.lbl_score_j4.Name = "lbl_score_j4"
        Me.lbl_score_j4.Size = New System.Drawing.Size(16, 26)
        Me.lbl_score_j4.TabIndex = 1
        Me.lbl_score_j4.Text = "4"
        Me.lbl_score_j4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur3
        '
        Me.tlp_joueur3.ColumnCount = 2
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Controls.Add(Me.lbl_nom_j3, 0, 0)
        Me.tlp_joueur3.Controls.Add(Me.lbl_score_j3, 1, 0)
        Me.tlp_joueur3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur3.Location = New System.Drawing.Point(437, 2)
        Me.tlp_joueur3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur3.Name = "tlp_joueur3"
        Me.tlp_joueur3.RowCount = 1
        Me.tlp_joueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Size = New System.Drawing.Size(211, 26)
        Me.tlp_joueur3.TabIndex = 4
        '
        'lbl_nom_j3
        '
        Me.lbl_nom_j3.AutoSize = True
        Me.lbl_nom_j3.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j3.Location = New System.Drawing.Point(38, 0)
        Me.lbl_nom_j3.Name = "lbl_nom_j3"
        Me.lbl_nom_j3.Size = New System.Drawing.Size(64, 26)
        Me.lbl_nom_j3.TabIndex = 0
        Me.lbl_nom_j3.Text = "Joueur 3"
        Me.lbl_nom_j3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j3
        '
        Me.lbl_score_j3.AutoSize = True
        Me.lbl_score_j3.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j3.Location = New System.Drawing.Point(108, 0)
        Me.lbl_score_j3.Name = "lbl_score_j3"
        Me.lbl_score_j3.Size = New System.Drawing.Size(16, 26)
        Me.lbl_score_j3.TabIndex = 1
        Me.lbl_score_j3.Text = "3"
        Me.lbl_score_j3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_plateau_jeu
        '
        Me.tlp_plateau_jeu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp_plateau_jeu.ColumnCount = 1
        Me.tlp_plateau_jeu.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_plateau_jeu.Controls.Add(Me.grb_scores, 0, 0)
        Me.tlp_plateau_jeu.Controls.Add(Me.tlp_commandes, 0, 2)
        Me.tlp_plateau_jeu.Controls.Add(Me.tlp_grille, 0, 1)
        Me.tlp_plateau_jeu.Location = New System.Drawing.Point(0, 0)
        Me.tlp_plateau_jeu.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_plateau_jeu.Name = "tlp_plateau_jeu"
        Me.tlp_plateau_jeu.RowCount = 3
        Me.tlp_plateau_jeu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.384615!))
        Me.tlp_plateau_jeu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.96335!))
        Me.tlp_plateau_jeu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.78709!))
        Me.tlp_plateau_jeu.Size = New System.Drawing.Size(882, 573)
        Me.tlp_plateau_jeu.TabIndex = 3
        '
        'tlp_commandes
        '
        Me.tlp_commandes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp_commandes.ColumnCount = 4
        Me.tlp_commandes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.47431!))
        Me.tlp_commandes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.17523!))
        Me.tlp_commandes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.17523!))
        Me.tlp_commandes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.17523!))
        Me.tlp_commandes.Controls.Add(Me.cmd_echange, 1, 0)
        Me.tlp_commandes.Controls.Add(Me.cmd_valider, 3, 0)
        Me.tlp_commandes.Controls.Add(Me.cmd_annuler, 2, 0)
        Me.tlp_commandes.Controls.Add(Me.tlp_stocktuiles, 0, 0)
        Me.tlp_commandes.Location = New System.Drawing.Point(3, 495)
        Me.tlp_commandes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_commandes.MaximumSize = New System.Drawing.Size(9999999, 80)
        Me.tlp_commandes.Name = "tlp_commandes"
        Me.tlp_commandes.RowCount = 1
        Me.tlp_commandes.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_commandes.Size = New System.Drawing.Size(876, 76)
        Me.tlp_commandes.TabIndex = 3
        '
        'cmd_echange
        '
        Me.cmd_echange.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_echange.Location = New System.Drawing.Point(541, 16)
        Me.cmd_echange.Margin = New System.Windows.Forms.Padding(12, 16, 12, 16)
        Me.cmd_echange.Name = "cmd_echange"
        Me.cmd_echange.Size = New System.Drawing.Size(91, 44)
        Me.cmd_echange.TabIndex = 0
        Me.cmd_echange.Text = "Echanger"
        Me.cmd_echange.UseVisualStyleBackColor = True
        '
        'cmd_valider
        '
        Me.cmd_valider.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_valider.Location = New System.Drawing.Point(771, 16)
        Me.cmd_valider.Margin = New System.Windows.Forms.Padding(12, 16, 12, 16)
        Me.cmd_valider.Name = "cmd_valider"
        Me.cmd_valider.Size = New System.Drawing.Size(93, 44)
        Me.cmd_valider.TabIndex = 2
        Me.cmd_valider.Text = "Valider"
        Me.cmd_valider.UseVisualStyleBackColor = True
        '
        'cmd_annuler
        '
        Me.cmd_annuler.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_annuler.Location = New System.Drawing.Point(656, 16)
        Me.cmd_annuler.Margin = New System.Windows.Forms.Padding(12, 16, 12, 16)
        Me.cmd_annuler.Name = "cmd_annuler"
        Me.cmd_annuler.Size = New System.Drawing.Size(91, 44)
        Me.cmd_annuler.TabIndex = 1
        Me.cmd_annuler.Text = "Annuler"
        Me.cmd_annuler.UseVisualStyleBackColor = True
        '
        'tlp_stocktuiles
        '
        Me.tlp_stocktuiles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp_stocktuiles.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlp_stocktuiles.ColumnCount = 6
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.Location = New System.Drawing.Point(3, 3)
        Me.tlp_stocktuiles.MaximumSize = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.MinimumSize = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.Name = "tlp_stocktuiles"
        Me.tlp_stocktuiles.RowCount = 1
        Me.tlp_stocktuiles.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_stocktuiles.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.tlp_stocktuiles.Size = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.TabIndex = 3
        '
        'tlp_grille
        '
        Me.tlp_grille.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp_grille.AutoScroll = True
        Me.tlp_grille.ColumnCount = 3
        Me.tlp_grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 676.0!))
        Me.tlp_grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlp_grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlp_grille.Location = New System.Drawing.Point(3, 56)
        Me.tlp_grille.Name = "tlp_grille"
        Me.tlp_grille.RowCount = 3
        Me.tlp_grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_grille.Size = New System.Drawing.Size(876, 434)
        Me.tlp_grille.TabIndex = 4
        '
        'frm_PlateauJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 573)
        Me.Controls.Add(Me.tlp_plateau_jeu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MinimumSize = New System.Drawing.Size(900, 620)
        Me.Name = "frm_PlateauJeu"
        Me.Text = "Qwirkle - Plateau de Jeu"
        Me.grb_scores.ResumeLayout(False)
        Me.tlp_all_joueur.ResumeLayout(False)
        Me.tlp_joueur1.ResumeLayout(False)
        Me.tlp_joueur1.PerformLayout()
        Me.tlp_joueur2.ResumeLayout(False)
        Me.tlp_joueur2.PerformLayout()
        Me.tlp_joueur4.ResumeLayout(False)
        Me.tlp_joueur4.PerformLayout()
        Me.tlp_joueur3.ResumeLayout(False)
        Me.tlp_joueur3.PerformLayout()
        Me.tlp_plateau_jeu.ResumeLayout(False)
        Me.tlp_commandes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbl_score_j1 As Label
    Friend WithEvents lbl_nom_j1 As Label
    Friend WithEvents grb_scores As GroupBox
    Friend WithEvents lbl_score_j4 As Label
    Friend WithEvents lbl_nom_j4 As Label
    Friend WithEvents lbl_score_j3 As Label
    Friend WithEvents lbl_nom_j3 As Label
    Friend WithEvents lbl_score_j2 As Label
    Friend WithEvents lbl_nom_j2 As Label
    Friend WithEvents tlp_plateau_jeu As TableLayoutPanel
    Friend WithEvents tlp_joueur3 As TableLayoutPanel
    Friend WithEvents tlp_joueur4 As TableLayoutPanel
    Friend WithEvents tlp_joueur2 As TableLayoutPanel
    Friend WithEvents tlp_joueur1 As TableLayoutPanel
    Friend WithEvents tlp_commandes As TableLayoutPanel
    Friend WithEvents cmd_echange As Button
    Friend WithEvents cmd_valider As Button
    Friend WithEvents cmd_annuler As Button
    Friend WithEvents tlp_all_joueur As TableLayoutPanel
    Friend WithEvents tlp_grille As TableLayoutPanel
    Friend WithEvents tlp_stocktuiles As TableLayoutPanel
End Class
