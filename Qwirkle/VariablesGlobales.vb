﻿Imports QwirkleEngine

Public Class VariablesGlobales
    Public Shared nbrJoueurs As Integer
    Public Shared nomsJoueurs As New List(Of String)
    Public Shared coteTuile As Integer = 60
    Public Shared tuilesJoueurEchange As List(Of Tuile)
    Public Shared casesCliqueesEchange As List(Of PBCase)
    Public Shared partieQuandElleEstFinie As Partie
End Class
