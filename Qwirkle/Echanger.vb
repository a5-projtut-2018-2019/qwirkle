﻿Imports QwirkleEngine

Public Class frm_Echanger
    Private Sub cmd_valider_echange_Click(sender As Object, e As EventArgs) Handles cmd_valider_echange.Click
        Close()
    End Sub

    Private Sub cmd_annuler_echange_Click(sender As Object, e As EventArgs) Handles cmd_annuler_echange.Click
        VariablesGlobales.casesCliqueesEchange.Clear()
        Close()
    End Sub

    Private Sub PBCase_Reserve_Click(sender As PBCase, e As EventArgs)
        sender.ToggleEstClique()
        If sender.GetEstClique() Then
            VariablesGlobales.casesCliqueesEchange.Add(sender)
        Else
            VariablesGlobales.casesCliqueesEchange.Remove(sender)
        End If
    End Sub

    Public Sub Rendre6Tuiles(ByRef tlp_destination As TableLayoutPanel, ByVal tuiles As List(Of Tuile))
        tlp_destination.Controls.Clear()
        Dim tlpIndex As Integer
        For tlpIndex = 0 To tuiles.Count - 1
            Dim couleur As Tuile.Couleur = tuiles(tlpIndex).GetCouleur()
            Dim forme As Tuile.Forme = tuiles(tlpIndex).GetForme()
            Dim caseAffichage As New PBCase(tuiles(tlpIndex), 0, 0)
            caseAffichage.BackColor = Color.Transparent
            AddHandler caseAffichage.Click, AddressOf Me.PBCase_Reserve_Click
            tlp_destination.Controls.Add(caseAffichage, tlpIndex, 0)
        Next
    End Sub

    Private Sub Frm_Echanger_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Rendre6Tuiles(tlp_stocktuiles, VariablesGlobales.tuilesJoueurEchange)
        VariablesGlobales.casesCliqueesEchange = New List(Of PBCase)
    End Sub
End Class