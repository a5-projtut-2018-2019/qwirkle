﻿Imports System.Collections.Generic
Imports QwirkleEngine

Public Class frm_Joueur
    Private Sub Frm_Joueur_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Select Case VariablesGlobales.nbrJoueurs
            Case 2
                txt_joueur3.Text = "Joueur 3"
                tlp_joueur3.Enabled = False
                txt_joueur4.Text = "Joueur 4"
                tlp_joueur4.Enabled = False
            Case 3
                txt_joueur4.Text = "Joueur 4"
                tlp_joueur4.Enabled = False
        End Select
    End Sub

    Private Function CompterTextboxVides() As Integer
        Dim indexTextBox As Integer
        Dim nbrTextboxVides As Integer = 0
        For indexTextBox = 1 To 4
            If Me.Controls("tlp_joueur" & indexTextBox).Controls("txt_joueur" & indexTextBox).Text = "" Then
                nbrTextboxVides += 1
            End If
        Next
        Return nbrTextboxVides
    End Function

    Private Function VerifierPseudosEgaux() As Boolean
        Dim listePseudos As New List(Of TextBox)
        listePseudos.Add(txt_joueur1)
        listePseudos.Add(txt_joueur2)
        listePseudos.Add(txt_joueur3)
        listePseudos.Add(txt_joueur4)

        Dim index As Integer
        For index = 1 To 4
            If tlp_principale.Controls("tlp_joueur" & index).Controls("txt_joueur" & index).Text = "" Then
                tlp_principale.Controls("tlp_joueur" & index).Controls("txt_joueur" & index).Text = "Joueur" & index
            End If
        Next

        Dim aPseudosEgaux As Boolean = False
        For Each pseudo As TextBox In listePseudos
            Dim autresPseudos As New List(Of TextBox)
            For Each pseudoCopie As TextBox In listePseudos
                autresPseudos.Add(pseudoCopie)
            Next
            autresPseudos.Remove(pseudo)

            For Each autrePseudo As TextBox In autresPseudos
                If pseudo.Text = autrePseudo.Text Then
                    aPseudosEgaux = True
                End If
            Next
        Next

        Return aPseudosEgaux
    End Function

    Private Sub cmd_valider_nom_Click(sender As Object, e As EventArgs) Handles cmd_valider_nom.Click
        If Not VerifierPseudosEgaux() Then
            Dim index As Integer
            For index = 1 To VariablesGlobales.nbrJoueurs
                VariablesGlobales.nomsJoueurs.Add(tlp_principale.Controls("tlp_joueur" & index).Controls("txt_joueur" & index).Text)
            Next

            frm_PlateauJeu.Show()
            Me.Close()
        Else
            MsgBox("Il ne peut pas y avoir de pseudos égaux", vbOKOnly + vbCritical, "Erreur")
        End If
    End Sub
End Class