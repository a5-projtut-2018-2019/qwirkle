﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ResumerFinPartie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ResumerFinPartie))
        Me.tlp_titre_tableau_score_félicitations_cmd = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_score_fin_partie = New System.Windows.Forms.Label()
        Me.tlp_scores_stats = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_tuiles_échangées = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_tuiles_échangées = New System.Windows.Forms.Label()
        Me.lbl_tuilesechangees = New System.Windows.Forms.Label()
        Me.tlp_joueur4 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j4 = New System.Windows.Forms.Label()
        Me.lbl_score_j4 = New System.Windows.Forms.Label()
        Me.tlp_points_max = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_points_max = New System.Windows.Forms.Label()
        Me.lbl_ptsmaxuncoup = New System.Windows.Forms.Label()
        Me.tlp_joueur3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j3 = New System.Windows.Forms.Label()
        Me.lbl_score_j3 = New System.Windows.Forms.Label()
        Me.tlp_points_moy = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_points_moy = New System.Windows.Forms.Label()
        Me.lbl_ptsmoyparcoup = New System.Windows.Forms.Label()
        Me.tlp_joueur2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j2 = New System.Windows.Forms.Label()
        Me.lbl_score_j2 = New System.Windows.Forms.Label()
        Me.tlp_nb_coups_joués = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nb_coups_joués = New System.Windows.Forms.Label()
        Me.lbl_coupsjoues = New System.Windows.Forms.Label()
        Me.tlp_joueur1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_nom_j1 = New System.Windows.Forms.Label()
        Me.lbl_score_j1 = New System.Windows.Forms.Label()
        Me.tlp_stat_valeur = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_statistique = New System.Windows.Forms.Label()
        Me.lbl_valeur = New System.Windows.Forms.Label()
        Me.tlp_joueur_score = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_joueur = New System.Windows.Forms.Label()
        Me.lbl_score = New System.Windows.Forms.Label()
        Me.lbl_félicitations = New System.Windows.Forms.Label()
        Me.tlp_cmd_save_rejouer_quitter = New System.Windows.Forms.TableLayoutPanel()
        Me.cmd_quitter_jeu = New System.Windows.Forms.Button()
        Me.cmd_rejouer = New System.Windows.Forms.Button()
        Me.tlp_save_open_score = New System.Windows.Forms.TableLayoutPanel()
        Me.cmd_save_score = New System.Windows.Forms.Button()
        Me.cmd_ouvrir_dossier_score = New System.Windows.Forms.Button()
        Me.FrmPlateauJeuBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tlp_titre_tableau_score_félicitations_cmd.SuspendLayout()
        Me.tlp_scores_stats.SuspendLayout()
        Me.tlp_tuiles_échangées.SuspendLayout()
        Me.tlp_joueur4.SuspendLayout()
        Me.tlp_points_max.SuspendLayout()
        Me.tlp_joueur3.SuspendLayout()
        Me.tlp_points_moy.SuspendLayout()
        Me.tlp_joueur2.SuspendLayout()
        Me.tlp_nb_coups_joués.SuspendLayout()
        Me.tlp_joueur1.SuspendLayout()
        Me.tlp_stat_valeur.SuspendLayout()
        Me.tlp_joueur_score.SuspendLayout()
        Me.tlp_cmd_save_rejouer_quitter.SuspendLayout()
        Me.tlp_save_open_score.SuspendLayout()
        CType(Me.FrmPlateauJeuBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlp_titre_tableau_score_félicitations_cmd
        '
        Me.tlp_titre_tableau_score_félicitations_cmd.ColumnCount = 1
        Me.tlp_titre_tableau_score_félicitations_cmd.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_titre_tableau_score_félicitations_cmd.Controls.Add(Me.lbl_score_fin_partie, 0, 0)
        Me.tlp_titre_tableau_score_félicitations_cmd.Controls.Add(Me.tlp_scores_stats, 0, 1)
        Me.tlp_titre_tableau_score_félicitations_cmd.Controls.Add(Me.lbl_félicitations, 0, 2)
        Me.tlp_titre_tableau_score_félicitations_cmd.Controls.Add(Me.tlp_cmd_save_rejouer_quitter, 0, 3)
        Me.tlp_titre_tableau_score_félicitations_cmd.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_titre_tableau_score_félicitations_cmd.Location = New System.Drawing.Point(0, 0)
        Me.tlp_titre_tableau_score_félicitations_cmd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_titre_tableau_score_félicitations_cmd.Name = "tlp_titre_tableau_score_félicitations_cmd"
        Me.tlp_titre_tableau_score_félicitations_cmd.RowCount = 4
        Me.tlp_titre_tableau_score_félicitations_cmd.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.17467!))
        Me.tlp_titre_tableau_score_félicitations_cmd.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.3391!))
        Me.tlp_titre_tableau_score_félicitations_cmd.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.24311!))
        Me.tlp_titre_tableau_score_félicitations_cmd.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.24311!))
        Me.tlp_titre_tableau_score_félicitations_cmd.Size = New System.Drawing.Size(846, 485)
        Me.tlp_titre_tableau_score_félicitations_cmd.TabIndex = 0
        '
        'lbl_score_fin_partie
        '
        Me.lbl_score_fin_partie.AutoSize = True
        Me.lbl_score_fin_partie.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_score_fin_partie.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_score_fin_partie.Location = New System.Drawing.Point(3, 0)
        Me.lbl_score_fin_partie.Name = "lbl_score_fin_partie"
        Me.lbl_score_fin_partie.Size = New System.Drawing.Size(840, 88)
        Me.lbl_score_fin_partie.TabIndex = 0
        Me.lbl_score_fin_partie.Text = "Score de fin de partie"
        Me.lbl_score_fin_partie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_scores_stats
        '
        Me.tlp_scores_stats.ColumnCount = 3
        Me.tlp_scores_stats.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.47826!))
        Me.tlp_scores_stats.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.04348!))
        Me.tlp_scores_stats.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.47826!))
        Me.tlp_scores_stats.Controls.Add(Me.tlp_tuiles_échangées, 2, 4)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_joueur4, 0, 4)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_points_max, 2, 3)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_joueur3, 0, 3)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_points_moy, 2, 2)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_joueur2, 0, 2)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_nb_coups_joués, 2, 1)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_joueur1, 0, 1)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_stat_valeur, 2, 0)
        Me.tlp_scores_stats.Controls.Add(Me.tlp_joueur_score, 0, 0)
        Me.tlp_scores_stats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_scores_stats.Location = New System.Drawing.Point(3, 90)
        Me.tlp_scores_stats.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_scores_stats.Name = "tlp_scores_stats"
        Me.tlp_scores_stats.RowCount = 5
        Me.tlp_scores_stats.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp_scores_stats.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp_scores_stats.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp_scores_stats.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp_scores_stats.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp_scores_stats.Size = New System.Drawing.Size(840, 186)
        Me.tlp_scores_stats.TabIndex = 1
        '
        'tlp_tuiles_échangées
        '
        Me.tlp_tuiles_échangées.ColumnCount = 2
        Me.tlp_tuiles_échangées.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_tuiles_échangées.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_tuiles_échangées.Controls.Add(Me.lbl_tuiles_échangées, 0, 0)
        Me.tlp_tuiles_échangées.Controls.Add(Me.lbl_tuilesechangees, 1, 0)
        Me.tlp_tuiles_échangées.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_tuiles_échangées.Location = New System.Drawing.Point(477, 150)
        Me.tlp_tuiles_échangées.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_tuiles_échangées.Name = "tlp_tuiles_échangées"
        Me.tlp_tuiles_échangées.RowCount = 1
        Me.tlp_tuiles_échangées.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_tuiles_échangées.Size = New System.Drawing.Size(360, 34)
        Me.tlp_tuiles_échangées.TabIndex = 14
        '
        'lbl_tuiles_échangées
        '
        Me.lbl_tuiles_échangées.AutoSize = True
        Me.lbl_tuiles_échangées.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_tuiles_échangées.Location = New System.Drawing.Point(57, 0)
        Me.lbl_tuiles_échangées.Name = "lbl_tuiles_échangées"
        Me.lbl_tuiles_échangées.Size = New System.Drawing.Size(120, 34)
        Me.lbl_tuiles_échangées.TabIndex = 0
        Me.lbl_tuiles_échangées.Text = "Tuiles échangées"
        Me.lbl_tuiles_échangées.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_tuilesechangees
        '
        Me.lbl_tuilesechangees.AutoSize = True
        Me.lbl_tuilesechangees.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_tuilesechangees.Location = New System.Drawing.Point(183, 0)
        Me.lbl_tuilesechangees.Name = "lbl_tuilesechangees"
        Me.lbl_tuilesechangees.Size = New System.Drawing.Size(16, 34)
        Me.lbl_tuilesechangees.TabIndex = 1
        Me.lbl_tuilesechangees.Text = "0"
        Me.lbl_tuilesechangees.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur4
        '
        Me.tlp_joueur4.ColumnCount = 2
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Controls.Add(Me.lbl_nom_j4, 0, 0)
        Me.tlp_joueur4.Controls.Add(Me.lbl_score_j4, 1, 0)
        Me.tlp_joueur4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur4.Location = New System.Drawing.Point(3, 150)
        Me.tlp_joueur4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur4.Name = "tlp_joueur4"
        Me.tlp_joueur4.RowCount = 1
        Me.tlp_joueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Size = New System.Drawing.Size(359, 34)
        Me.tlp_joueur4.TabIndex = 12
        '
        'lbl_nom_j4
        '
        Me.lbl_nom_j4.AutoSize = True
        Me.lbl_nom_j4.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j4.Location = New System.Drawing.Point(116, 0)
        Me.lbl_nom_j4.Name = "lbl_nom_j4"
        Me.lbl_nom_j4.Size = New System.Drawing.Size(60, 34)
        Me.lbl_nom_j4.TabIndex = 0
        Me.lbl_nom_j4.Text = "Joueur4"
        Me.lbl_nom_j4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j4
        '
        Me.lbl_score_j4.AutoSize = True
        Me.lbl_score_j4.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j4.Location = New System.Drawing.Point(182, 0)
        Me.lbl_score_j4.Name = "lbl_score_j4"
        Me.lbl_score_j4.Size = New System.Drawing.Size(16, 34)
        Me.lbl_score_j4.TabIndex = 1
        Me.lbl_score_j4.Text = "0"
        Me.lbl_score_j4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_points_max
        '
        Me.tlp_points_max.ColumnCount = 2
        Me.tlp_points_max.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_max.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_max.Controls.Add(Me.lbl_points_max, 0, 0)
        Me.tlp_points_max.Controls.Add(Me.lbl_ptsmaxuncoup, 1, 0)
        Me.tlp_points_max.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_points_max.Location = New System.Drawing.Point(477, 113)
        Me.tlp_points_max.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_points_max.Name = "tlp_points_max"
        Me.tlp_points_max.RowCount = 1
        Me.tlp_points_max.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_max.Size = New System.Drawing.Size(360, 33)
        Me.tlp_points_max.TabIndex = 11
        '
        'lbl_points_max
        '
        Me.lbl_points_max.AutoSize = True
        Me.lbl_points_max.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_points_max.Location = New System.Drawing.Point(26, 0)
        Me.lbl_points_max.Name = "lbl_points_max"
        Me.lbl_points_max.Size = New System.Drawing.Size(151, 33)
        Me.lbl_points_max.TabIndex = 0
        Me.lbl_points_max.Text = "Points max en un coup"
        Me.lbl_points_max.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ptsmaxuncoup
        '
        Me.lbl_ptsmaxuncoup.AutoSize = True
        Me.lbl_ptsmaxuncoup.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_ptsmaxuncoup.Location = New System.Drawing.Point(183, 0)
        Me.lbl_ptsmaxuncoup.Name = "lbl_ptsmaxuncoup"
        Me.lbl_ptsmaxuncoup.Size = New System.Drawing.Size(16, 33)
        Me.lbl_ptsmaxuncoup.TabIndex = 1
        Me.lbl_ptsmaxuncoup.Text = "0"
        Me.lbl_ptsmaxuncoup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur3
        '
        Me.tlp_joueur3.ColumnCount = 2
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Controls.Add(Me.lbl_nom_j3, 0, 0)
        Me.tlp_joueur3.Controls.Add(Me.lbl_score_j3, 1, 0)
        Me.tlp_joueur3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur3.Location = New System.Drawing.Point(3, 113)
        Me.tlp_joueur3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur3.Name = "tlp_joueur3"
        Me.tlp_joueur3.RowCount = 1
        Me.tlp_joueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Size = New System.Drawing.Size(359, 33)
        Me.tlp_joueur3.TabIndex = 9
        '
        'lbl_nom_j3
        '
        Me.lbl_nom_j3.AutoSize = True
        Me.lbl_nom_j3.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j3.Location = New System.Drawing.Point(116, 0)
        Me.lbl_nom_j3.Name = "lbl_nom_j3"
        Me.lbl_nom_j3.Size = New System.Drawing.Size(60, 33)
        Me.lbl_nom_j3.TabIndex = 0
        Me.lbl_nom_j3.Text = "Joueur3"
        Me.lbl_nom_j3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j3
        '
        Me.lbl_score_j3.AutoSize = True
        Me.lbl_score_j3.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j3.Location = New System.Drawing.Point(182, 0)
        Me.lbl_score_j3.Name = "lbl_score_j3"
        Me.lbl_score_j3.Size = New System.Drawing.Size(16, 33)
        Me.lbl_score_j3.TabIndex = 1
        Me.lbl_score_j3.Text = "0"
        Me.lbl_score_j3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_points_moy
        '
        Me.tlp_points_moy.ColumnCount = 2
        Me.tlp_points_moy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_moy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_moy.Controls.Add(Me.lbl_points_moy, 0, 0)
        Me.tlp_points_moy.Controls.Add(Me.lbl_ptsmoyparcoup, 1, 0)
        Me.tlp_points_moy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_points_moy.Location = New System.Drawing.Point(477, 76)
        Me.tlp_points_moy.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_points_moy.Name = "tlp_points_moy"
        Me.tlp_points_moy.RowCount = 1
        Me.tlp_points_moy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_points_moy.Size = New System.Drawing.Size(360, 33)
        Me.tlp_points_moy.TabIndex = 8
        '
        'lbl_points_moy
        '
        Me.lbl_points_moy.AutoSize = True
        Me.lbl_points_moy.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_points_moy.Location = New System.Drawing.Point(17, 0)
        Me.lbl_points_moy.Name = "lbl_points_moy"
        Me.lbl_points_moy.Size = New System.Drawing.Size(160, 33)
        Me.lbl_points_moy.TabIndex = 0
        Me.lbl_points_moy.Text = "Points moyens par coup"
        Me.lbl_points_moy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ptsmoyparcoup
        '
        Me.lbl_ptsmoyparcoup.AutoSize = True
        Me.lbl_ptsmoyparcoup.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_ptsmoyparcoup.Location = New System.Drawing.Point(183, 0)
        Me.lbl_ptsmoyparcoup.Name = "lbl_ptsmoyparcoup"
        Me.lbl_ptsmoyparcoup.Size = New System.Drawing.Size(16, 33)
        Me.lbl_ptsmoyparcoup.TabIndex = 1
        Me.lbl_ptsmoyparcoup.Text = "0"
        Me.lbl_ptsmoyparcoup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur2
        '
        Me.tlp_joueur2.ColumnCount = 2
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Controls.Add(Me.lbl_nom_j2, 0, 0)
        Me.tlp_joueur2.Controls.Add(Me.lbl_score_j2, 1, 0)
        Me.tlp_joueur2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur2.Location = New System.Drawing.Point(3, 76)
        Me.tlp_joueur2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur2.Name = "tlp_joueur2"
        Me.tlp_joueur2.RowCount = 1
        Me.tlp_joueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Size = New System.Drawing.Size(359, 33)
        Me.tlp_joueur2.TabIndex = 6
        '
        'lbl_nom_j2
        '
        Me.lbl_nom_j2.AutoSize = True
        Me.lbl_nom_j2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j2.Location = New System.Drawing.Point(116, 0)
        Me.lbl_nom_j2.Name = "lbl_nom_j2"
        Me.lbl_nom_j2.Size = New System.Drawing.Size(60, 33)
        Me.lbl_nom_j2.TabIndex = 0
        Me.lbl_nom_j2.Text = "Joueur2"
        Me.lbl_nom_j2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j2
        '
        Me.lbl_score_j2.AutoSize = True
        Me.lbl_score_j2.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j2.Location = New System.Drawing.Point(182, 0)
        Me.lbl_score_j2.Name = "lbl_score_j2"
        Me.lbl_score_j2.Size = New System.Drawing.Size(16, 33)
        Me.lbl_score_j2.TabIndex = 1
        Me.lbl_score_j2.Text = "0"
        Me.lbl_score_j2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_nb_coups_joués
        '
        Me.tlp_nb_coups_joués.ColumnCount = 2
        Me.tlp_nb_coups_joués.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nb_coups_joués.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nb_coups_joués.Controls.Add(Me.lbl_nb_coups_joués, 0, 0)
        Me.tlp_nb_coups_joués.Controls.Add(Me.lbl_coupsjoues, 1, 0)
        Me.tlp_nb_coups_joués.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_nb_coups_joués.Location = New System.Drawing.Point(477, 39)
        Me.tlp_nb_coups_joués.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_nb_coups_joués.Name = "tlp_nb_coups_joués"
        Me.tlp_nb_coups_joués.RowCount = 1
        Me.tlp_nb_coups_joués.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nb_coups_joués.Size = New System.Drawing.Size(360, 33)
        Me.tlp_nb_coups_joués.TabIndex = 5
        '
        'lbl_nb_coups_joués
        '
        Me.lbl_nb_coups_joués.AutoSize = True
        Me.lbl_nb_coups_joués.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nb_coups_joués.Location = New System.Drawing.Point(19, 0)
        Me.lbl_nb_coups_joués.Name = "lbl_nb_coups_joués"
        Me.lbl_nb_coups_joués.Size = New System.Drawing.Size(158, 33)
        Me.lbl_nb_coups_joués.TabIndex = 0
        Me.lbl_nb_coups_joués.Text = "Nombre de coups joués"
        Me.lbl_nb_coups_joués.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_coupsjoues
        '
        Me.lbl_coupsjoues.AutoSize = True
        Me.lbl_coupsjoues.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_coupsjoues.Location = New System.Drawing.Point(183, 0)
        Me.lbl_coupsjoues.Name = "lbl_coupsjoues"
        Me.lbl_coupsjoues.Size = New System.Drawing.Size(16, 33)
        Me.lbl_coupsjoues.TabIndex = 1
        Me.lbl_coupsjoues.Text = "0"
        Me.lbl_coupsjoues.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur1
        '
        Me.tlp_joueur1.ColumnCount = 2
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Controls.Add(Me.lbl_nom_j1, 0, 0)
        Me.tlp_joueur1.Controls.Add(Me.lbl_score_j1, 1, 0)
        Me.tlp_joueur1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur1.Location = New System.Drawing.Point(3, 39)
        Me.tlp_joueur1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur1.Name = "tlp_joueur1"
        Me.tlp_joueur1.RowCount = 1
        Me.tlp_joueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Size = New System.Drawing.Size(359, 33)
        Me.tlp_joueur1.TabIndex = 3
        '
        'lbl_nom_j1
        '
        Me.lbl_nom_j1.AutoSize = True
        Me.lbl_nom_j1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_nom_j1.Location = New System.Drawing.Point(116, 0)
        Me.lbl_nom_j1.Name = "lbl_nom_j1"
        Me.lbl_nom_j1.Size = New System.Drawing.Size(60, 33)
        Me.lbl_nom_j1.TabIndex = 0
        Me.lbl_nom_j1.Text = "Joueur1"
        Me.lbl_nom_j1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score_j1
        '
        Me.lbl_score_j1.AutoSize = True
        Me.lbl_score_j1.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score_j1.Location = New System.Drawing.Point(182, 0)
        Me.lbl_score_j1.Name = "lbl_score_j1"
        Me.lbl_score_j1.Size = New System.Drawing.Size(16, 33)
        Me.lbl_score_j1.TabIndex = 1
        Me.lbl_score_j1.Text = "0"
        Me.lbl_score_j1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_stat_valeur
        '
        Me.tlp_stat_valeur.ColumnCount = 2
        Me.tlp_stat_valeur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_stat_valeur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_stat_valeur.Controls.Add(Me.lbl_statistique, 0, 0)
        Me.tlp_stat_valeur.Controls.Add(Me.lbl_valeur, 1, 0)
        Me.tlp_stat_valeur.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_stat_valeur.Location = New System.Drawing.Point(477, 2)
        Me.tlp_stat_valeur.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_stat_valeur.Name = "tlp_stat_valeur"
        Me.tlp_stat_valeur.RowCount = 1
        Me.tlp_stat_valeur.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_stat_valeur.Size = New System.Drawing.Size(360, 33)
        Me.tlp_stat_valeur.TabIndex = 2
        '
        'lbl_statistique
        '
        Me.lbl_statistique.AutoSize = True
        Me.lbl_statistique.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_statistique.Location = New System.Drawing.Point(103, 0)
        Me.lbl_statistique.Name = "lbl_statistique"
        Me.lbl_statistique.Size = New System.Drawing.Size(74, 33)
        Me.lbl_statistique.TabIndex = 0
        Me.lbl_statistique.Text = "Statistique"
        Me.lbl_statistique.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_valeur
        '
        Me.lbl_valeur.AutoSize = True
        Me.lbl_valeur.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_valeur.Location = New System.Drawing.Point(183, 0)
        Me.lbl_valeur.Name = "lbl_valeur"
        Me.lbl_valeur.Size = New System.Drawing.Size(49, 33)
        Me.lbl_valeur.TabIndex = 1
        Me.lbl_valeur.Text = "Valeur"
        Me.lbl_valeur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_joueur_score
        '
        Me.tlp_joueur_score.ColumnCount = 2
        Me.tlp_joueur_score.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur_score.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur_score.Controls.Add(Me.lbl_joueur, 0, 0)
        Me.tlp_joueur_score.Controls.Add(Me.lbl_score, 1, 0)
        Me.tlp_joueur_score.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur_score.Location = New System.Drawing.Point(3, 2)
        Me.tlp_joueur_score.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur_score.Name = "tlp_joueur_score"
        Me.tlp_joueur_score.RowCount = 1
        Me.tlp_joueur_score.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur_score.Size = New System.Drawing.Size(359, 33)
        Me.tlp_joueur_score.TabIndex = 0
        '
        'lbl_joueur
        '
        Me.lbl_joueur.AutoSize = True
        Me.lbl_joueur.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueur.Location = New System.Drawing.Point(124, 0)
        Me.lbl_joueur.Name = "lbl_joueur"
        Me.lbl_joueur.Size = New System.Drawing.Size(52, 33)
        Me.lbl_joueur.TabIndex = 0
        Me.lbl_joueur.Text = "Joueur"
        Me.lbl_joueur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_score
        '
        Me.lbl_score.AutoSize = True
        Me.lbl_score.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbl_score.Location = New System.Drawing.Point(182, 0)
        Me.lbl_score.Name = "lbl_score"
        Me.lbl_score.Size = New System.Drawing.Size(45, 33)
        Me.lbl_score.TabIndex = 1
        Me.lbl_score.Text = "Score"
        Me.lbl_score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_félicitations
        '
        Me.lbl_félicitations.AutoSize = True
        Me.lbl_félicitations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_félicitations.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_félicitations.Location = New System.Drawing.Point(27, 302)
        Me.lbl_félicitations.Margin = New System.Windows.Forms.Padding(27, 24, 27, 24)
        Me.lbl_félicitations.Name = "lbl_félicitations"
        Me.lbl_félicitations.Size = New System.Drawing.Size(792, 55)
        Me.lbl_félicitations.TabIndex = 2
        Me.lbl_félicitations.Text = "Félicitations à "
        Me.lbl_félicitations.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tlp_cmd_save_rejouer_quitter
        '
        Me.tlp_cmd_save_rejouer_quitter.ColumnCount = 3
        Me.tlp_cmd_save_rejouer_quitter.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_cmd_save_rejouer_quitter.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_cmd_save_rejouer_quitter.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_cmd_save_rejouer_quitter.Controls.Add(Me.cmd_quitter_jeu, 2, 0)
        Me.tlp_cmd_save_rejouer_quitter.Controls.Add(Me.cmd_rejouer, 1, 0)
        Me.tlp_cmd_save_rejouer_quitter.Controls.Add(Me.tlp_save_open_score, 0, 0)
        Me.tlp_cmd_save_rejouer_quitter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_cmd_save_rejouer_quitter.Location = New System.Drawing.Point(3, 383)
        Me.tlp_cmd_save_rejouer_quitter.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_cmd_save_rejouer_quitter.Name = "tlp_cmd_save_rejouer_quitter"
        Me.tlp_cmd_save_rejouer_quitter.RowCount = 1
        Me.tlp_cmd_save_rejouer_quitter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666!))
        Me.tlp_cmd_save_rejouer_quitter.Size = New System.Drawing.Size(840, 100)
        Me.tlp_cmd_save_rejouer_quitter.TabIndex = 3
        '
        'cmd_quitter_jeu
        '
        Me.cmd_quitter_jeu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_quitter_jeu.Location = New System.Drawing.Point(649, 24)
        Me.cmd_quitter_jeu.Margin = New System.Windows.Forms.Padding(89, 24, 88, 24)
        Me.cmd_quitter_jeu.Name = "cmd_quitter_jeu"
        Me.cmd_quitter_jeu.Size = New System.Drawing.Size(103, 52)
        Me.cmd_quitter_jeu.TabIndex = 2
        Me.cmd_quitter_jeu.Text = "Quitter"
        Me.cmd_quitter_jeu.UseVisualStyleBackColor = True
        '
        'cmd_rejouer
        '
        Me.cmd_rejouer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_rejouer.Location = New System.Drawing.Point(369, 24)
        Me.cmd_rejouer.Margin = New System.Windows.Forms.Padding(89, 24, 88, 24)
        Me.cmd_rejouer.Name = "cmd_rejouer"
        Me.cmd_rejouer.Size = New System.Drawing.Size(103, 52)
        Me.cmd_rejouer.TabIndex = 1
        Me.cmd_rejouer.Text = "Rejouer"
        Me.cmd_rejouer.UseVisualStyleBackColor = True
        '
        'tlp_save_open_score
        '
        Me.tlp_save_open_score.ColumnCount = 1
        Me.tlp_save_open_score.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_save_open_score.Controls.Add(Me.cmd_save_score, 0, 0)
        Me.tlp_save_open_score.Controls.Add(Me.cmd_ouvrir_dossier_score, 0, 1)
        Me.tlp_save_open_score.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_save_open_score.Location = New System.Drawing.Point(3, 2)
        Me.tlp_save_open_score.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_save_open_score.Name = "tlp_save_open_score"
        Me.tlp_save_open_score.RowCount = 2
        Me.tlp_save_open_score.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_save_open_score.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_save_open_score.Size = New System.Drawing.Size(274, 96)
        Me.tlp_save_open_score.TabIndex = 3
        '
        'cmd_save_score
        '
        Me.cmd_save_score.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_save_score.Location = New System.Drawing.Point(27, 8)
        Me.cmd_save_score.Margin = New System.Windows.Forms.Padding(27, 8, 27, 8)
        Me.cmd_save_score.Name = "cmd_save_score"
        Me.cmd_save_score.Size = New System.Drawing.Size(220, 32)
        Me.cmd_save_score.TabIndex = 0
        Me.cmd_save_score.Text = "Sauvegarder les scores"
        Me.cmd_save_score.UseVisualStyleBackColor = True
        '
        'cmd_ouvrir_dossier_score
        '
        Me.cmd_ouvrir_dossier_score.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_ouvrir_dossier_score.Location = New System.Drawing.Point(27, 56)
        Me.cmd_ouvrir_dossier_score.Margin = New System.Windows.Forms.Padding(27, 8, 27, 8)
        Me.cmd_ouvrir_dossier_score.Name = "cmd_ouvrir_dossier_score"
        Me.cmd_ouvrir_dossier_score.Size = New System.Drawing.Size(220, 32)
        Me.cmd_ouvrir_dossier_score.TabIndex = 1
        Me.cmd_ouvrir_dossier_score.Text = "Ouvrir le dossier des scores"
        Me.cmd_ouvrir_dossier_score.UseVisualStyleBackColor = True
        '
        'FrmPlateauJeuBindingSource
        '
        Me.FrmPlateauJeuBindingSource.DataSource = GetType(Qwirkle.frm_PlateauJeu)
        '
        'frm_ResumerFinPartie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 485)
        Me.Controls.Add(Me.tlp_titre_tableau_score_félicitations_cmd)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximumSize = New System.Drawing.Size(864, 532)
        Me.MinimumSize = New System.Drawing.Size(864, 532)
        Me.Name = "frm_ResumerFinPartie"
        Me.Text = "Qwirkle - Scores"
        Me.tlp_titre_tableau_score_félicitations_cmd.ResumeLayout(False)
        Me.tlp_titre_tableau_score_félicitations_cmd.PerformLayout()
        Me.tlp_scores_stats.ResumeLayout(False)
        Me.tlp_tuiles_échangées.ResumeLayout(False)
        Me.tlp_tuiles_échangées.PerformLayout()
        Me.tlp_joueur4.ResumeLayout(False)
        Me.tlp_joueur4.PerformLayout()
        Me.tlp_points_max.ResumeLayout(False)
        Me.tlp_points_max.PerformLayout()
        Me.tlp_joueur3.ResumeLayout(False)
        Me.tlp_joueur3.PerformLayout()
        Me.tlp_points_moy.ResumeLayout(False)
        Me.tlp_points_moy.PerformLayout()
        Me.tlp_joueur2.ResumeLayout(False)
        Me.tlp_joueur2.PerformLayout()
        Me.tlp_nb_coups_joués.ResumeLayout(False)
        Me.tlp_nb_coups_joués.PerformLayout()
        Me.tlp_joueur1.ResumeLayout(False)
        Me.tlp_joueur1.PerformLayout()
        Me.tlp_stat_valeur.ResumeLayout(False)
        Me.tlp_stat_valeur.PerformLayout()
        Me.tlp_joueur_score.ResumeLayout(False)
        Me.tlp_joueur_score.PerformLayout()
        Me.tlp_cmd_save_rejouer_quitter.ResumeLayout(False)
        Me.tlp_save_open_score.ResumeLayout(False)
        CType(Me.FrmPlateauJeuBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents tlp_titre_tableau_score_félicitations_cmd As TableLayoutPanel
    Friend WithEvents lbl_score_fin_partie As Label
    Friend WithEvents FrmPlateauJeuBindingSource As BindingSource
    Friend WithEvents tlp_scores_stats As TableLayoutPanel
    Friend WithEvents tlp_joueur_score As TableLayoutPanel
    Friend WithEvents lbl_joueur As Label
    Friend WithEvents lbl_score As Label
    Friend WithEvents tlp_tuiles_échangées As TableLayoutPanel
    Friend WithEvents lbl_tuiles_échangées As Label
    Friend WithEvents lbl_tuilesechangees As Label
    Friend WithEvents tlp_joueur4 As TableLayoutPanel
    Friend WithEvents lbl_nom_j4 As Label
    Friend WithEvents lbl_score_j4 As Label
    Friend WithEvents tlp_points_max As TableLayoutPanel
    Friend WithEvents lbl_points_max As Label
    Friend WithEvents lbl_ptsmaxuncoup As Label
    Friend WithEvents tlp_joueur3 As TableLayoutPanel
    Friend WithEvents lbl_nom_j3 As Label
    Friend WithEvents lbl_score_j3 As Label
    Friend WithEvents tlp_points_moy As TableLayoutPanel
    Friend WithEvents lbl_points_moy As Label
    Friend WithEvents lbl_ptsmoyparcoup As Label
    Friend WithEvents tlp_joueur2 As TableLayoutPanel
    Friend WithEvents lbl_nom_j2 As Label
    Friend WithEvents lbl_score_j2 As Label
    Friend WithEvents tlp_nb_coups_joués As TableLayoutPanel
    Friend WithEvents lbl_nb_coups_joués As Label
    Friend WithEvents lbl_coupsjoues As Label
    Friend WithEvents tlp_joueur1 As TableLayoutPanel
    Friend WithEvents lbl_nom_j1 As Label
    Friend WithEvents lbl_score_j1 As Label
    Friend WithEvents tlp_stat_valeur As TableLayoutPanel
    Friend WithEvents lbl_statistique As Label
    Friend WithEvents lbl_valeur As Label
    Friend WithEvents lbl_félicitations As Label
    Friend WithEvents tlp_cmd_save_rejouer_quitter As TableLayoutPanel
    Friend WithEvents cmd_quitter_jeu As Button
    Friend WithEvents cmd_rejouer As Button
    Friend WithEvents cmd_save_score As Button
    Friend WithEvents tlp_save_open_score As TableLayoutPanel
    Friend WithEvents cmd_ouvrir_dossier_score As Button
End Class
