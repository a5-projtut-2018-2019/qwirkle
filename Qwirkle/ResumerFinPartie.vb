﻿Imports System.Text
Imports QwirkleEngine

Public Class frm_ResumerFinPartie
    Private Sub cmd_quitter_jeu_Click(sender As Object, e As EventArgs) Handles cmd_quitter_jeu.Click
        Me.Close()
    End Sub

    Private Sub cmd_rejouer_Click(sender As Object, e As EventArgs) Handles cmd_rejouer.Click
        frm_Accueil.Show()
        Me.Close()
    End Sub

    Private Sub cmd_save_score_Click(sender As Object, e As EventArgs) Handles cmd_save_score.Click
        Dim dateAsText = DateTime.Now.ToString("dd-MM-yyyy HH_mm_ss")
        Dim StrBuild As New StringBuilder
        Dim fileName = "Score " + dateAsText + ".txt"
        Dim Save_Score As New IO.StreamWriter("..\..\..\Save_Scores_Partie\" + fileName)
        StrBuild.Append(String.Format("{0}", lbl_score_fin_partie.Text)).AppendLine()
        StrBuild.AppendLine()
        StrBuild.Append(String.Format("| {0}     | {1} |", lbl_joueur.Text, lbl_score.Text)).AppendLine()
        If frm_Accueil.nud_joueurs.Value = 2 Then
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j1.Text, lbl_score_j1.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j2.Text, lbl_score_j2.Text)).AppendLine()
        ElseIf frm_Accueil.nud_joueurs.Value = 3 Then
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j1.Text, lbl_score_j1.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j2.Text, lbl_score_j2.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j3.Text, lbl_score_j3.Text)).AppendLine()
        ElseIf frm_Accueil.nud_joueurs.Value = 4 Then
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j1.Text, lbl_score_j1.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j2.Text, lbl_score_j2.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j3.Text, lbl_score_j3.Text)).AppendLine()
            StrBuild.Append(String.Format("| {0}   | {1}     |", lbl_nom_j4.Text, lbl_score_j4.Text)).AppendLine()
        End If
        StrBuild.AppendLine()
        StrBuild.Append(String.Format("| {0}            | {1} |", lbl_statistique.Text, lbl_valeur.Text)).AppendLine()
        StrBuild.Append(String.Format("| {0}  | {1}     |", lbl_nb_coups_joués.Text, lbl_coupsjoues.Text)).AppendLine()
        StrBuild.Append(String.Format("| {0} | {1}      |", lbl_points_moy.Text, lbl_ptsmoyparcoup.Text)).AppendLine()
        StrBuild.Append(String.Format("| {0}  | {1}     |", lbl_points_max.Text, lbl_ptsmaxuncoup.Text)).AppendLine()
        StrBuild.Append(String.Format("| {0}       | {1}     |", lbl_tuiles_échangées.Text, lbl_tuilesechangees.Text)).AppendLine()
        StrBuild.AppendLine()
        StrBuild.Append(String.Format("{0}", lbl_félicitations.Text)).AppendLine()
        Dim Texte_Save As String = StrBuild.ToString()
        Save_Score.WriteLine(Texte_Save)
        Save_Score.Close()
        MessageBox.Show("Score enregistrer dans un fichier texte." + vbNewLine + "Chemin d'accés :" + vbNewLine + "qwirkle\Save_Scores_Partie\", "Enregistrer")
    End Sub

    Private Sub cmd_ouvrir_dossier_score_Click(sender As Object, e As EventArgs) Handles cmd_ouvrir_dossier_score.Click
        Shell("C:\windows\explorer.exe ..\..\..\Save_Scores_Partie", vbNormalFocus)
    End Sub

    Private Sub frm_ResumerFinPartie_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim index As Integer
        For index = 0 To VariablesGlobales.partieQuandElleEstFinie.GetListeJoueursOrdonnee().Count - 1
            Dim joueur As Joueur = VariablesGlobales.partieQuandElleEstFinie.GetListeJoueursOrdonnee()(index)
            tlp_scores_stats.Controls("tlp_joueur" & index + 1).Controls("lbl_nom_j" & index + 1).Text = joueur.GetNom()
            tlp_scores_stats.Controls("tlp_joueur" & index + 1).Controls("lbl_score_j" & index + 1).Text = joueur.GetPoints()
        Next

        lbl_points_max.Text = VariablesGlobales.partieQuandElleEstFinie.GetMaxPointsEnUnCoup()
        lbl_points_moy.Text = VariablesGlobales.partieQuandElleEstFinie.GetPointsMoyensParTour()
        lbl_ptsmaxuncoup.Text = VariablesGlobales.partieQuandElleEstFinie.GetMaxPointsEnUnCoup()
        lbl_tuilesechangees.Text = VariablesGlobales.partieQuandElleEstFinie.GetNbrTuilesEchangees()
    End Sub
End Class