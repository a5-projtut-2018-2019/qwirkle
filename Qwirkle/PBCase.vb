﻿Imports QwirkleEngine
Imports System.Resources

Public Class PBCase
    Inherits PictureBox

    Private tuile As Tuile
    Dim estClique As Boolean
    Dim x, y As Integer

    Sub New(ByVal tuile As Tuile, ByVal x As Integer, ByVal y As Integer)
        Me.BackColor = Color.White
        Me.Width = Me.Height = 50
        Me.Anchor = AnchorStyles.Bottom + AnchorStyles.Left + AnchorStyles.Top + AnchorStyles.Right
        Me.SizeMode = PictureBoxSizeMode.StretchImage

        If tuile IsNot Nothing Then
            Me.Image = My.Resources.ResourceManager.GetObject(tuile.GetForme().ToString() & tuile.GetCouleur().ToString())
        End If

        Me.tuile = tuile
        Me.estClique = False
        Me.x = x
        Me.y = y
    End Sub

    Function GetCouleur() As Tuile.Couleur
        Return tuile.GetCouleur()
    End Function

    Function GetForme() As Tuile.Forme
        Return tuile.GetForme()
    End Function

    Function GetEstNull() As Boolean
        If tuile Is Nothing Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub ToggleEstClique()
        Select Case Me.estClique
            Case True
                Me.estClique = False
                Me.BackColor = Color.Transparent
            Case False
                Me.estClique = True
                Me.BackColor = Color.DarkGray
        End Select
    End Sub

    Function GetEstClique() As Boolean
        Return Me.estClique
    End Function

    Function GetX() As Integer
        Return Me.x
    End Function

    Function GetY() As Integer
        Return Me.y
    End Function

    Function GetTuile() As Tuile
        Return Me.tuile
    End Function

    Sub EstPosee()
        Me.BackColor = Color.Black
    End Sub

    Sub EstValide(ByVal estValide As Boolean)
        If estValide Then
            Me.BackColor = Color.Green
        Else
            Me.BackColor = Color.White
        End If
    End Sub
End Class
