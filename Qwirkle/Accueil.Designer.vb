﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Accueil))
        Me.lbl_Qwirkle = New System.Windows.Forms.Label()
        Me.cmd_saisir_nom = New System.Windows.Forms.Button()
        Me.tlp_principale_saisir_nom = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_nombre_joueur = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_joueurs = New System.Windows.Forms.Label()
        Me.nud_joueurs = New System.Windows.Forms.NumericUpDown()
        Me.tlp_principale_saisir_nom.SuspendLayout()
        Me.tlp_nombre_joueur.SuspendLayout()
        CType(Me.nud_joueurs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_Qwirkle
        '
        Me.lbl_Qwirkle.AutoSize = True
        Me.lbl_Qwirkle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_Qwirkle.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Qwirkle.Location = New System.Drawing.Point(4, 0)
        Me.lbl_Qwirkle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_Qwirkle.Name = "lbl_Qwirkle"
        Me.lbl_Qwirkle.Size = New System.Drawing.Size(873, 161)
        Me.lbl_Qwirkle.TabIndex = 0
        Me.lbl_Qwirkle.Text = "Qwirkle"
        Me.lbl_Qwirkle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmd_saisir_nom
        '
        Me.cmd_saisir_nom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_saisir_nom.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_saisir_nom.Location = New System.Drawing.Point(267, 354)
        Me.cmd_saisir_nom.Margin = New System.Windows.Forms.Padding(267, 32, 267, 32)
        Me.cmd_saisir_nom.Name = "cmd_saisir_nom"
        Me.cmd_saisir_nom.Size = New System.Drawing.Size(347, 99)
        Me.cmd_saisir_nom.TabIndex = 1
        Me.cmd_saisir_nom.Text = "Saisir les noms des joueurs"
        Me.cmd_saisir_nom.UseVisualStyleBackColor = True
        '
        'tlp_principale_saisir_nom
        '
        Me.tlp_principale_saisir_nom.ColumnCount = 1
        Me.tlp_principale_saisir_nom.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_principale_saisir_nom.Controls.Add(Me.lbl_Qwirkle, 0, 0)
        Me.tlp_principale_saisir_nom.Controls.Add(Me.cmd_saisir_nom, 0, 2)
        Me.tlp_principale_saisir_nom.Controls.Add(Me.tlp_nombre_joueur, 0, 1)
        Me.tlp_principale_saisir_nom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_principale_saisir_nom.Location = New System.Drawing.Point(0, 0)
        Me.tlp_principale_saisir_nom.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_principale_saisir_nom.Name = "tlp_principale_saisir_nom"
        Me.tlp_principale_saisir_nom.RowCount = 3
        Me.tlp_principale_saisir_nom.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_principale_saisir_nom.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_principale_saisir_nom.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tlp_principale_saisir_nom.Size = New System.Drawing.Size(881, 485)
        Me.tlp_principale_saisir_nom.TabIndex = 5
        '
        'tlp_nombre_joueur
        '
        Me.tlp_nombre_joueur.ColumnCount = 2
        Me.tlp_nombre_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nombre_joueur.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nombre_joueur.Controls.Add(Me.lbl_joueurs, 0, 0)
        Me.tlp_nombre_joueur.Controls.Add(Me.nud_joueurs, 1, 0)
        Me.tlp_nombre_joueur.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_nombre_joueur.Location = New System.Drawing.Point(3, 163)
        Me.tlp_nombre_joueur.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_nombre_joueur.Name = "tlp_nombre_joueur"
        Me.tlp_nombre_joueur.RowCount = 1
        Me.tlp_nombre_joueur.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_nombre_joueur.Size = New System.Drawing.Size(875, 157)
        Me.tlp_nombre_joueur.TabIndex = 6
        '
        'lbl_joueurs
        '
        Me.lbl_joueurs.AutoSize = True
        Me.lbl_joueurs.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueurs.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_joueurs.Location = New System.Drawing.Point(191, 48)
        Me.lbl_joueurs.Margin = New System.Windows.Forms.Padding(169, 48, 4, 40)
        Me.lbl_joueurs.Name = "lbl_joueurs"
        Me.lbl_joueurs.Size = New System.Drawing.Size(242, 69)
        Me.lbl_joueurs.TabIndex = 2
        Me.lbl_joueurs.Text = "Nombre de joueurs :"
        '
        'nud_joueurs
        '
        Me.nud_joueurs.Dock = System.Windows.Forms.DockStyle.Left
        Me.nud_joueurs.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nud_joueurs.Location = New System.Drawing.Point(441, 112)
        Me.nud_joueurs.Margin = New System.Windows.Forms.Padding(4, 112, 356, 0)
        Me.nud_joueurs.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nud_joueurs.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nud_joueurs.Name = "nud_joueurs"
        Me.nud_joueurs.Size = New System.Drawing.Size(71, 36)
        Me.nud_joueurs.TabIndex = 4
        Me.nud_joueurs.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'frm_Accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 485)
        Me.Controls.Add(Me.tlp_principale_saisir_nom)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(899, 532)
        Me.Name = "frm_Accueil"
        Me.Text = "Qwirkle - Paramètre"
        Me.tlp_principale_saisir_nom.ResumeLayout(False)
        Me.tlp_principale_saisir_nom.PerformLayout()
        Me.tlp_nombre_joueur.ResumeLayout(False)
        Me.tlp_nombre_joueur.PerformLayout()
        CType(Me.nud_joueurs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lbl_Qwirkle As Label
    Friend WithEvents cmd_saisir_nom As Button
    Friend WithEvents tlp_principale_saisir_nom As TableLayoutPanel
    Friend WithEvents tlp_nombre_joueur As TableLayoutPanel
    Friend WithEvents lbl_joueurs As Label
    Friend WithEvents nud_joueurs As NumericUpDown
End Class
