﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Echanger
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Echanger))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.gb_mespieces = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmd_valider_echange = New System.Windows.Forms.Button()
        Me.cmd_annuler_echange = New System.Windows.Forms.Button()
        Me.tlp_stocktuiles = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gb_mespieces.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gb_mespieces, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(582, 253)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'gb_mespieces
        '
        Me.gb_mespieces.Controls.Add(Me.tlp_stocktuiles)
        Me.gb_mespieces.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gb_mespieces.Location = New System.Drawing.Point(3, 2)
        Me.gb_mespieces.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.gb_mespieces.Name = "gb_mespieces"
        Me.gb_mespieces.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.gb_mespieces.Size = New System.Drawing.Size(576, 185)
        Me.gb_mespieces.TabIndex = 0
        Me.gb_mespieces.TabStop = False
        Me.gb_mespieces.Text = "Mes pièces"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmd_valider_echange, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmd_annuler_echange, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 191)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(576, 60)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'cmd_valider_echange
        '
        Me.cmd_valider_echange.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_valider_echange.Location = New System.Drawing.Point(50, 10)
        Me.cmd_valider_echange.Margin = New System.Windows.Forms.Padding(50, 10, 50, 10)
        Me.cmd_valider_echange.Name = "cmd_valider_echange"
        Me.cmd_valider_echange.Size = New System.Drawing.Size(188, 40)
        Me.cmd_valider_echange.TabIndex = 0
        Me.cmd_valider_echange.Text = "Valider"
        Me.cmd_valider_echange.UseVisualStyleBackColor = True
        '
        'cmd_annuler_echange
        '
        Me.cmd_annuler_echange.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_annuler_echange.Location = New System.Drawing.Point(338, 10)
        Me.cmd_annuler_echange.Margin = New System.Windows.Forms.Padding(50, 10, 50, 10)
        Me.cmd_annuler_echange.Name = "cmd_annuler_echange"
        Me.cmd_annuler_echange.Size = New System.Drawing.Size(188, 40)
        Me.cmd_annuler_echange.TabIndex = 1
        Me.cmd_annuler_echange.Text = "Annuler"
        Me.cmd_annuler_echange.UseVisualStyleBackColor = True
        '
        'tlp_stocktuiles
        '
        Me.tlp_stocktuiles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp_stocktuiles.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlp_stocktuiles.ColumnCount = 6
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_stocktuiles.Location = New System.Drawing.Point(82, 60)
        Me.tlp_stocktuiles.MaximumSize = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.MinimumSize = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.Name = "tlp_stocktuiles"
        Me.tlp_stocktuiles.RowCount = 1
        Me.tlp_stocktuiles.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_stocktuiles.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.tlp_stocktuiles.Size = New System.Drawing.Size(420, 70)
        Me.tlp_stocktuiles.TabIndex = 4
        '
        'frm_Echanger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(582, 253)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximumSize = New System.Drawing.Size(600, 300)
        Me.MinimumSize = New System.Drawing.Size(600, 300)
        Me.Name = "frm_Echanger"
        Me.Text = "Echanger"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gb_mespieces.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents gb_mespieces As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents cmd_valider_echange As Button
    Friend WithEvents cmd_annuler_echange As Button
    Friend WithEvents tlp_stocktuiles As TableLayoutPanel
End Class
