﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Joueur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Joueur))
        Me.txt_joueur1 = New System.Windows.Forms.TextBox()
        Me.lbl_joueur1 = New System.Windows.Forms.Label()
        Me.lbl_joueur2 = New System.Windows.Forms.Label()
        Me.txt_joueur2 = New System.Windows.Forms.TextBox()
        Me.lbl_joueur3 = New System.Windows.Forms.Label()
        Me.txt_joueur3 = New System.Windows.Forms.TextBox()
        Me.lbl_joueur4 = New System.Windows.Forms.Label()
        Me.txt_joueur4 = New System.Windows.Forms.TextBox()
        Me.lbl_Saisir_Nom = New System.Windows.Forms.Label()
        Me.cmd_valider_nom = New System.Windows.Forms.Button()
        Me.tlp_principale = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur4 = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur2 = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_joueur1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tlp_principale.SuspendLayout()
        Me.tlp_joueur4.SuspendLayout()
        Me.tlp_joueur3.SuspendLayout()
        Me.tlp_joueur2.SuspendLayout()
        Me.tlp_joueur1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_joueur1
        '
        Me.txt_joueur1.Dock = System.Windows.Forms.DockStyle.Left
        Me.txt_joueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_joueur1.Location = New System.Drawing.Point(357, 2)
        Me.txt_joueur1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_joueur1.Name = "txt_joueur1"
        Me.txt_joueur1.Size = New System.Drawing.Size(223, 36)
        Me.txt_joueur1.TabIndex = 0
        '
        'lbl_joueur1
        '
        Me.lbl_joueur1.AutoSize = True
        Me.lbl_joueur1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_joueur1.Location = New System.Drawing.Point(226, 0)
        Me.lbl_joueur1.Name = "lbl_joueur1"
        Me.lbl_joueur1.Size = New System.Drawing.Size(125, 77)
        Me.lbl_joueur1.TabIndex = 1
        Me.lbl_joueur1.Text = "Joueur 1 :"
        '
        'lbl_joueur2
        '
        Me.lbl_joueur2.AutoSize = True
        Me.lbl_joueur2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_joueur2.Location = New System.Drawing.Point(226, 0)
        Me.lbl_joueur2.Name = "lbl_joueur2"
        Me.lbl_joueur2.Size = New System.Drawing.Size(125, 77)
        Me.lbl_joueur2.TabIndex = 3
        Me.lbl_joueur2.Text = "Joueur 2 :"
        '
        'txt_joueur2
        '
        Me.txt_joueur2.Dock = System.Windows.Forms.DockStyle.Left
        Me.txt_joueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_joueur2.Location = New System.Drawing.Point(357, 2)
        Me.txt_joueur2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_joueur2.Name = "txt_joueur2"
        Me.txt_joueur2.Size = New System.Drawing.Size(223, 36)
        Me.txt_joueur2.TabIndex = 2
        '
        'lbl_joueur3
        '
        Me.lbl_joueur3.AutoSize = True
        Me.lbl_joueur3.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_joueur3.Location = New System.Drawing.Point(226, 0)
        Me.lbl_joueur3.Name = "lbl_joueur3"
        Me.lbl_joueur3.Size = New System.Drawing.Size(125, 77)
        Me.lbl_joueur3.TabIndex = 5
        Me.lbl_joueur3.Text = "Joueur 3 :"
        '
        'txt_joueur3
        '
        Me.txt_joueur3.Dock = System.Windows.Forms.DockStyle.Left
        Me.txt_joueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_joueur3.Location = New System.Drawing.Point(357, 2)
        Me.txt_joueur3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_joueur3.Name = "txt_joueur3"
        Me.txt_joueur3.Size = New System.Drawing.Size(223, 36)
        Me.txt_joueur3.TabIndex = 4
        '
        'lbl_joueur4
        '
        Me.lbl_joueur4.AutoSize = True
        Me.lbl_joueur4.Dock = System.Windows.Forms.DockStyle.Right
        Me.lbl_joueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_joueur4.Location = New System.Drawing.Point(226, 0)
        Me.lbl_joueur4.Name = "lbl_joueur4"
        Me.lbl_joueur4.Size = New System.Drawing.Size(125, 77)
        Me.lbl_joueur4.TabIndex = 7
        Me.lbl_joueur4.Text = "Joueur 4 :"
        '
        'txt_joueur4
        '
        Me.txt_joueur4.Dock = System.Windows.Forms.DockStyle.Left
        Me.txt_joueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_joueur4.Location = New System.Drawing.Point(357, 2)
        Me.txt_joueur4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_joueur4.Name = "txt_joueur4"
        Me.txt_joueur4.Size = New System.Drawing.Size(223, 36)
        Me.txt_joueur4.TabIndex = 6
        '
        'lbl_Saisir_Nom
        '
        Me.lbl_Saisir_Nom.AutoSize = True
        Me.lbl_Saisir_Nom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_Saisir_Nom.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Saisir_Nom.Location = New System.Drawing.Point(3, 0)
        Me.lbl_Saisir_Nom.Name = "lbl_Saisir_Nom"
        Me.lbl_Saisir_Nom.Size = New System.Drawing.Size(709, 81)
        Me.lbl_Saisir_Nom.TabIndex = 8
        Me.lbl_Saisir_Nom.Text = "Saisissez  le nom des joueurs"
        Me.lbl_Saisir_Nom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmd_valider_nom
        '
        Me.cmd_valider_nom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmd_valider_nom.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_valider_nom.Location = New System.Drawing.Point(178, 421)
        Me.cmd_valider_nom.Margin = New System.Windows.Forms.Padding(178, 16, 178, 16)
        Me.cmd_valider_nom.Name = "cmd_valider_nom"
        Me.cmd_valider_nom.Size = New System.Drawing.Size(359, 52)
        Me.cmd_valider_nom.TabIndex = 9
        Me.cmd_valider_nom.Text = "Lancer la partie"
        Me.cmd_valider_nom.UseVisualStyleBackColor = True
        '
        'tlp_principale
        '
        Me.tlp_principale.ColumnCount = 1
        Me.tlp_principale.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp_principale.Controls.Add(Me.tlp_joueur4, 0, 4)
        Me.tlp_principale.Controls.Add(Me.tlp_joueur3, 0, 3)
        Me.tlp_principale.Controls.Add(Me.tlp_joueur2, 0, 2)
        Me.tlp_principale.Controls.Add(Me.lbl_Saisir_Nom, 0, 0)
        Me.tlp_principale.Controls.Add(Me.tlp_joueur1, 0, 1)
        Me.tlp_principale.Controls.Add(Me.cmd_valider_nom, 0, 5)
        Me.tlp_principale.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_principale.Location = New System.Drawing.Point(0, 0)
        Me.tlp_principale.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_principale.Name = "tlp_principale"
        Me.tlp_principale.RowCount = 6
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.tlp_principale.Size = New System.Drawing.Size(715, 489)
        Me.tlp_principale.TabIndex = 10
        '
        'tlp_joueur4
        '
        Me.tlp_joueur4.ColumnCount = 2
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Controls.Add(Me.lbl_joueur4, 0, 0)
        Me.tlp_joueur4.Controls.Add(Me.txt_joueur4, 1, 0)
        Me.tlp_joueur4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur4.Location = New System.Drawing.Point(3, 326)
        Me.tlp_joueur4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur4.Name = "tlp_joueur4"
        Me.tlp_joueur4.RowCount = 1
        Me.tlp_joueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur4.Size = New System.Drawing.Size(709, 77)
        Me.tlp_joueur4.TabIndex = 11
        '
        'tlp_joueur3
        '
        Me.tlp_joueur3.ColumnCount = 2
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Controls.Add(Me.lbl_joueur3, 0, 0)
        Me.tlp_joueur3.Controls.Add(Me.txt_joueur3, 1, 0)
        Me.tlp_joueur3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur3.Location = New System.Drawing.Point(3, 245)
        Me.tlp_joueur3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur3.Name = "tlp_joueur3"
        Me.tlp_joueur3.RowCount = 1
        Me.tlp_joueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur3.Size = New System.Drawing.Size(709, 77)
        Me.tlp_joueur3.TabIndex = 12
        '
        'tlp_joueur2
        '
        Me.tlp_joueur2.ColumnCount = 2
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Controls.Add(Me.lbl_joueur2, 0, 0)
        Me.tlp_joueur2.Controls.Add(Me.txt_joueur2, 1, 0)
        Me.tlp_joueur2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur2.Location = New System.Drawing.Point(3, 164)
        Me.tlp_joueur2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur2.Name = "tlp_joueur2"
        Me.tlp_joueur2.RowCount = 1
        Me.tlp_joueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur2.Size = New System.Drawing.Size(709, 77)
        Me.tlp_joueur2.TabIndex = 13
        '
        'tlp_joueur1
        '
        Me.tlp_joueur1.ColumnCount = 2
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Controls.Add(Me.lbl_joueur1, 0, 0)
        Me.tlp_joueur1.Controls.Add(Me.txt_joueur1, 1, 0)
        Me.tlp_joueur1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_joueur1.Location = New System.Drawing.Point(3, 83)
        Me.tlp_joueur1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tlp_joueur1.Name = "tlp_joueur1"
        Me.tlp_joueur1.RowCount = 1
        Me.tlp_joueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlp_joueur1.Size = New System.Drawing.Size(709, 77)
        Me.tlp_joueur1.TabIndex = 12
        '
        'frm_Joueur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 489)
        Me.Controls.Add(Me.tlp_principale)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MinimumSize = New System.Drawing.Size(733, 536)
        Me.Name = "frm_Joueur"
        Me.Text = "Qwirkle - Joueur"
        Me.tlp_principale.ResumeLayout(False)
        Me.tlp_principale.PerformLayout()
        Me.tlp_joueur4.ResumeLayout(False)
        Me.tlp_joueur4.PerformLayout()
        Me.tlp_joueur3.ResumeLayout(False)
        Me.tlp_joueur3.PerformLayout()
        Me.tlp_joueur2.ResumeLayout(False)
        Me.tlp_joueur2.PerformLayout()
        Me.tlp_joueur1.ResumeLayout(False)
        Me.tlp_joueur1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txt_joueur1 As TextBox
    Friend WithEvents lbl_joueur1 As Label
    Friend WithEvents lbl_joueur2 As Label
    Friend WithEvents txt_joueur2 As TextBox
    Friend WithEvents lbl_joueur3 As Label
    Friend WithEvents txt_joueur3 As TextBox
    Friend WithEvents lbl_joueur4 As Label
    Friend WithEvents txt_joueur4 As TextBox
    Friend WithEvents lbl_Saisir_Nom As Label
    Friend WithEvents cmd_valider_nom As Button
    Friend WithEvents tlp_principale As TableLayoutPanel
    Friend WithEvents tlp_joueur4 As TableLayoutPanel
    Friend WithEvents tlp_joueur3 As TableLayoutPanel
    Friend WithEvents tlp_joueur2 As TableLayoutPanel
    Friend WithEvents tlp_joueur1 As TableLayoutPanel

End Class
