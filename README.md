# Projet Tutore : Qwirkle

#### But du Qwirkle

C'est d’aligner des tuiles qui doivent être identiques soit par leur symbole ou par leur couleur,
de façon à réaliser des combinaisons rapportant un maximum de points.

## Diagramme UML

![Diagramme UML](img/uml.png)

## Utilisation

Soon

## Auteurs
- [Charles Berdugo](https://gitlab.com/CharlesBdg)
- [Jacoillot Victor](https://gitlab.com/victor_jacoillot)
- [Louvet Quentin](https://gitlab.com/QuentinLouvet)
- [Sobolewski Louis-Baptiste](https://gitlab.com/louis-baptiste.sobolewski)