﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestCase
    {
        [TestMethod]
        public void TestCaseConstructeur()
        {
            Case @case = new Case(3, 2);
            Tuile tuile = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Assert.AreEqual(null, @case.GetTuile());
            @case.SetTuile(tuile);
            Assert.AreEqual(tuile, @case.GetTuile());
        }
        [TestMethod]
        public void TestEnleverTuile()
        {
            Case @case = new Case(3, 2);
            Tuile tuile = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            @case.SetTuile(tuile);
            @case.EnleverTuile();
            Assert.AreEqual(null, @case.GetTuile());
        }
    }
}
