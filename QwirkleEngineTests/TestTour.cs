﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestTour
    {
        [TestMethod]
        public void TestAnnulerPlacements()
        {
            Joueur bob = new Joueur("Bob");
            Tour tour = new Tour(bob, 3, 3);
            Tuile tuile = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            tour.PlacerTuile(tuile, 1, 1);            
            tour.AnnulerPlacements();
            Assert.AreEqual(null, tour.GetTuile(1, 1));
        }

        [TestMethod]
        public void TestPlacerTuile()
        {
            Joueur bob = new Joueur("Bob");
            Tour tour = new Tour(bob, 3, 3);
            Tuile tuile = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            tour.PlacerTuile(tuile, 1, 1);
            Assert.AreEqual(tuile, tour.GetTuile(1, 1));
        }

        [TestMethod]
        public void TestEchangerTuiles()
        {
            Joueur bob = new Joueur("Bob");
            List<Joueur> Joueurs = new List<Joueur>();
            Joueurs.Add(bob);
            Partie partieTest = new Partie(Joueurs);
            partieTest.GetStockTuiles().GetTuiles().Clear();
            bob.GetReserve().GetTuiles().Clear();
            Tour tour = partieTest.GetTourActuel();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Cercle);
            Tuile tuile3 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Trefle);
            bob.AjouterTuile(tuile1);
            partieTest.GetStockTuiles().AjouterTuile(tuile2);
            partieTest.GetStockTuiles().AjouterTuile(tuile3);
            tour.EchangerTuiles(bob.GetTuiles());
            partieTest.ValiderTour();
            Assert.AreEqual(3, bob.GetTuiles().Count);
        }
    }
}
