﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestJoueur
    {
        [TestMethod]
        public void TestAjouterPoints()
        {
            Joueur bob = new Joueur("Bob");
            bob.AjouterPoints(3);
            Assert.AreEqual(3, bob.GetPoints());
        }
        [TestMethod]
        public void TestJoueurConstructeur()
        {
            Joueur bob = new Joueur("Bob");
            Assert.AreEqual("Bob", bob.GetNom());
            Assert.AreEqual(0, bob.GetPoints());
        }
        [TestMethod]
        public void TestAjouterTuile()
        {
            Joueur bob = new Joueur("Bob");
            Tuile tuile1 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            bob.AjouterTuile(tuile1);
            Assert.AreEqual(1, bob.GetTuiles().Count);
            Assert.AreEqual(tuile1, bob.GetTuiles()[0]);
        }
    }
}
