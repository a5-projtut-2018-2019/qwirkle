﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestUtilitaires
    {
        [TestMethod]
        public void TestRemplirReserveJoueur()
        {
            ReserveTuilesJoueur ReserveBob = new ReserveTuilesJoueur();
            ReserveTuilesJoueur ReserveRegis = new ReserveTuilesJoueur();
            ReserveTuilesPartie ReservePartie = new ReserveTuilesPartie();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Losange);
            Tuile tuile3 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Trefle);
            Tuile tuile4 = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Losange);
            Tuile tuile5 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Etoile4);
            Tuile tuile6 = new Tuile(Tuile.Couleur.Vert, Tuile.Forme.Etoile8);
            Tuile tuile7 = new Tuile(Tuile.Couleur.Violet, Tuile.Forme.Carre);
            Tuile tuile8 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Cercle);
            Tuile tuile9 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Etoile8);
            Tuile tuile10 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuile11 = new Tuile(Tuile.Couleur.Violet, Tuile.Forme.Trefle);
            //Initialisation ReservePartie & ReserveRegis
            ReservePartie.AjouterTuile(tuile1);
            ReservePartie.AjouterTuile(tuile2);
            ReservePartie.AjouterTuile(tuile3);
            ReservePartie.AjouterTuile(tuile4);
            ReservePartie.AjouterTuile(tuile5);
            ReservePartie.AjouterTuile(tuile6);
            ReservePartie.AjouterTuile(tuile7);
            ReservePartie.AjouterTuile(tuile8);
            ReservePartie.AjouterTuile(tuile9);
            ReserveRegis.AjouterTuile(tuile10);
            ReserveRegis.AjouterTuile(tuile11);
            // Cas ReserveTuilesJoueur = 0 & ReservePartie = 9
            Utilitaires.RemplirReserveJoueur(ReserveBob, ReservePartie);
            Assert.AreEqual(6, ReserveBob.GetTuiles().Count);
            // Cas ReserveTuilesJoueur = 2 & ReservePartie = 3
            Utilitaires.RemplirReserveJoueur(ReserveRegis, ReservePartie);
            Assert.AreEqual(5, ReserveRegis.GetTuiles().Count);
        }
        [TestMethod]
        public void TestEchangeEntreReserves()
        {
            ReserveTuilesJoueur ReserveBob = new ReserveTuilesJoueur();
            ReserveTuilesPartie ReservePartie = new ReserveTuilesPartie();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Losange);
            ReserveBob.AjouterTuile(tuile1);
            ReservePartie.AjouterTuile(tuile2);
            Utilitaires.EchangeEntreReserves(ReserveBob, ReservePartie, tuile1);
            Assert.AreEqual(2, ReservePartie.GetTuiles().Count);
            Assert.AreEqual(0, ReserveBob.GetTuiles().Count);
        }
    }
}
