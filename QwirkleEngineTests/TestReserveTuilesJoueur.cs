﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestReserveTuilesJoueur
    {
        [TestMethod]
        public void TestEchangerTuile()
        {
            Joueur bob = new Joueur("Bob");
            ReserveTuilesJoueur ReserveBob = new ReserveTuilesJoueur();
            Tuile tuiletemporaire = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuilerandom = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Etoile4);
            ReserveBob.AjouterTuile(tuiletemporaire);
            ReserveBob.EchangerTuile(tuiletemporaire);
            Assert.AreEqual(tuilerandom, bob.GetTuiles()[0]);
        }

        [TestMethod]
        public void TestRemplir()
        {
            Joueur bob = new Joueur("Bob");
            ReserveTuilesJoueur ReserveBob = new ReserveTuilesJoueur();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Losange);
            Tuile tuile3 = new Tuile(Tuile.Couleur.Vert, Tuile.Forme.Etoile4);
            Tuile tuile4 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Trefle);
            ReserveBob.Remplir();
            Assert.AreEqual(tuile3, bob.GetTuiles()[2]);
            Assert.AreNotEqual(tuile1, bob.GetTuiles()[1]);
        }

        [TestMethod]
        public void TestAjouterTuile()
        {
            Joueur bob = new Joueur("Bob");
            ReserveTuilesJoueur ReserveBob = new ReserveTuilesJoueur();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Losange);
            ReserveBob.AjouterTuile(tuile1);
            Assert.AreEqual(tuile1, bob.GetTuiles()[0]);
            Assert.AreNotEqual(tuile1, bob.GetTuiles()[1]);
        }
    }
}
