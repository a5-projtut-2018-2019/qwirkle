﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using QwirkleEngine;


namespace QwirkleEngineTests
{
    [TestClass]
    public class TestPartie
    {
        
        [TestMethod]
        public void TestCalculerOrdreJoueurs()
        {
            Joueur bob = new Joueur("Bob");
            Joueur regis = new Joueur("Regis");
            Joueur philibert = new Joueur("Philibert");
            List<Joueur> Joueurs = new List<Joueur>();
            Joueurs.Add(bob);
            Joueurs.Add(philibert);
            Joueurs.Add(regis);
            Partie partieTest = new Partie(Joueurs);
            List<Joueur> joueurOrdonnes = partieTest.GetListeJoueursOrdonnee();
            Assert.AreEqual(3, joueurOrdonnes.Count);
            Assert.AreNotEqual(joueurOrdonnes[0].GetNom(), joueurOrdonnes[1].GetNom());
            Assert.AreNotEqual(joueurOrdonnes[0].GetNom(), joueurOrdonnes[2].GetNom());
            Assert.AreNotEqual(joueurOrdonnes[1].GetNom(), joueurOrdonnes[2].GetNom());
        }
        [TestMethod]
        public void TestEstTerminee()
        {
            Joueur bob = new Joueur("Bob");
            Joueur regis = new Joueur("Regis");
            List<Joueur> Joueurs = new List<Joueur>();
            Joueurs.Add(bob);
            Joueurs.Add(regis);
            Partie partieTest = new Partie(Joueurs);
            // > 0 tuiles restantes
            Assert.AreEqual(false, partieTest.EstTerminee());
            // 0 tuiles restantes
            partieTest.GetStockTuiles().GetTuiles().Clear();
            bob.GetReserve().GetTuiles().Clear();
            regis.GetReserve().GetTuiles().Clear();
            Assert.AreEqual(true, partieTest.EstTerminee());
        }
        [TestMethod]
        public void TestValiderTour()
        {
            Joueur bob = new Joueur("Bob");
            Joueur regis = new Joueur("Regis");
            List<Joueur> Joueurs = new List<Joueur>();
            Joueurs.Add(bob);
            Joueurs.Add(regis);
            Partie partieTest = new Partie(Joueurs);
            Tuile tuile1 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Cercle);
            partieTest.GetTourActuel().PlacerTuile(tuile1, 2, 1);
            partieTest.ValiderTour();
            partieTest.GetTourActuel().PlacerTuile(tuile2, 2, 2);
            partieTest.ValiderTour();
            Assert.AreEqual(tuile1,partieTest.GetGrille().GetTuile(2, 1));
            Assert.AreEqual(tuile2, partieTest.GetGrille().GetTuile(2, 2));
            Assert.AreEqual(1, partieTest.GetListeJoueursOrdonnee()[0].GetPoints());
            Assert.AreEqual(2, partieTest.GetListeJoueursOrdonnee()[1].GetPoints());
        }
        [TestMethod]
        public void TestVerifierPlacerTuile()
        {
            Joueur bob = new Joueur("Bob");
            List<Joueur> Joueurs = new List<Joueur>();
            Joueurs.Add(bob);
            Partie partie = new Partie(Joueurs);
            // 1er tour
            Tuile tuile1 = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Etoile4);
            Tuile tuile2 = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Losange);
            Tuile tuile3 = new Tuile(Tuile.Couleur.Rouge, Tuile.Forme.Cercle);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile1, 1, 1));
            partie.GetTourActuel().PlacerTuile(tuile1, 1, 1);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile2, 1, 2));
            partie.GetTourActuel().PlacerTuile(tuile2, 1, 2);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile3, 1, 3));
            partie.GetTourActuel().PlacerTuile(tuile3, 1, 3);
            partie.ValiderTour();
            // 2e tour
            Tuile tuile4 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Cercle);
            Tuile tuile5 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Cercle);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile4, 0, 3));
            partie.GetTourActuel().PlacerTuile(tuile4, 0, 3);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile5, 3, 3)); 
            partie.GetTourActuel().PlacerTuile(tuile5, 3, 3);
            partie.ValiderTour();
            // 3e tour
            Tuile tuile6 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Trefle);
            Tuile tuile7 = new Tuile(Tuile.Couleur.Orange, Tuile.Forme.Losange);
            Tuile tuilefausse1 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Losange);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile6, 1, 4));
            partie.GetTourActuel().PlacerTuile(tuile6, 1, 4);
            Assert.AreEqual(true, partie.VerifierPlacerTuile(tuile7, 1, 5));
            partie.GetTourActuel().PlacerTuile(tuile7, 1, 5);
            Assert.AreEqual(false, partie.VerifierPlacerTuile(tuilefausse1, 1, 6));
            partie.ValiderTour();
        }
        
    }
}