﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestReserveTuilesPartie
    {
        [TestMethod]
        public void TestInitReserve()
        {
            ReserveTuilesPartie ReservePartie = new ReserveTuilesPartie();
            ReservePartie.InitReserve();
            Assert.AreEqual(108, ReservePartie.GetNbrTuilesRestantes());
            Assert.AreEqual(18, ReservePartie.GetNbrTuilesRestantesCouleur(Tuile.Couleur.Rouge));
            Assert.AreEqual(18, ReservePartie.GetNbrTuilesRestantesForme(Tuile.Forme.Cercle));
        }
    }
}
