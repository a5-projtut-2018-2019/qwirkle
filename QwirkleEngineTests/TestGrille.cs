﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleEngine;

namespace QwirkleEngineTests
{
    [TestClass]
    public class TestGrille
    {
        [TestMethod]
        public void TestPlacerTuile()
        {
            Grille grille = new Grille();
            Tuile tuile = new Tuile(Tuile.Couleur.Jaune, Tuile.Forme.Carre);
            grille.PlacerTuile(tuile, 1, 1);
            Assert.AreEqual(tuile, grille.GetTuile(1, 1));
        }
        [TestMethod]
        public void TestViderTuiles()
        {
            Grille grilleTest = new Grille();
            Tuile tuile1 = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            grilleTest.PlacerTuile(tuile1, 1, 1);
            grilleTest.ViderTuiles();
            Assert.AreEqual(null, grilleTest.GetTuile(1, 1));
        }
        [TestMethod]
        public void TestAgrandirGrilleGauche()
        {
            Grille grille = new Grille();
            Tuile tuile = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            grille.PlacerTuile(tuile, 0, 1);
            Assert.AreEqual(4, grille.GetLargeur());
        }
        [TestMethod]
        public void TestAgrandirGrilleDroite()
        {
            Grille grille = new Grille();
            Tuile tuile = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            grille.PlacerTuile(tuile, 2, 1);
            Assert.AreEqual(4, grille.GetLargeur());
        }
        [TestMethod]
        public void TestAgrandirGrilleBas()
        {
            Grille grille = new Grille();
            Tuile tuile = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            grille.PlacerTuile(tuile, 1, 0);
            Assert.AreEqual(4, grille.GetHauteur());
        }
        [TestMethod]
        public void TestAgrandirGrilleHaut()
        {
            Grille grille = new Grille();
            Tuile tuile = new Tuile(Tuile.Couleur.Bleu, Tuile.Forme.Carre);
            grille.PlacerTuile(tuile, 1, 2);
            Assert.AreEqual(4, grille.GetHauteur());
        }
    }
}
