﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Utilitaires
    {
        public static void EchangeEntreReserves(ReserveTuiles source, ReserveTuiles destination, Tuile tuile)
        {
            source.EnleverTuile(tuile);
            destination.AjouterTuile(tuile);
        }

        public static void RemplirReserveJoueur(ReserveTuilesJoueur reserveJoueur, ReserveTuilesPartie reservePartie)
        {
            int nbrTuilesDisponibles = reservePartie.GetNbrTuilesRestantes();

            if (nbrTuilesDisponibles > 0)
            {
                int nbrTuilesManquantes = 6 - reserveJoueur.GetTuiles().Count;
                int nbrTuilesAjoutees;

                if (nbrTuilesManquantes > nbrTuilesDisponibles)
                {
                    nbrTuilesAjoutees = nbrTuilesDisponibles;
                }
                else
                {
                    nbrTuilesAjoutees = nbrTuilesManquantes;
                }

                Random aleatoire = new Random();

                for (int iteration = 0; iteration < nbrTuilesAjoutees; iteration++)
                {
                    List<Tuile> tuilesPartie = reservePartie.GetTuiles();
                    int indexAleatoire = aleatoire.Next(nbrTuilesDisponibles - 1);
                    nbrTuilesDisponibles--;
                    Tuile tuile = tuilesPartie[indexAleatoire];
                    reservePartie.EnleverTuile(tuile);
                    reserveJoueur.AjouterTuile(tuile);
                }
            }
        }
    }
}
