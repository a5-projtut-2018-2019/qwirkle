﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Tour
    {
        private Grille grille = new Grille();
        private Joueur joueur;
        private List<Case> casesModifiees = new List<Case>();
        private List<Tuile> tuilesUtilisees = new List<Tuile>();
        private int points;
        private bool isTourEchange = false;
        private int nbrTuilesEchangees = 0;
        private int initialX = 0, initialY = 0;

        public Tour(Joueur joueur, int x, int y)
        {
            initialX = x;
            initialY = y;
            this.joueur = joueur;
            Case[,] tableauCases = new Case[x, y];
            for (int largeur = 0; largeur < x; largeur++)
            {
                for (int hauteur = 0; hauteur < y; hauteur++)
                {
                    tableauCases[largeur, hauteur] = new Case(largeur, hauteur);
                }
            }
            grille.SetCases(tableauCases);
        }

        public Joueur GetJoueur()
        {
            return this.joueur;
        }

        public bool IsTourEchange()
        {
            return isTourEchange;
        }

        public void EchangerTuiles(List<Tuile> tuiles)
        {
            SetIsTourEchange(true, tuiles.Count);
            foreach(Tuile tuile in tuiles)
            {
                tuilesUtilisees.Add(tuile);
            }
        }

        public List<Tuile> GetTuilesUtilisees()
        {
            return tuilesUtilisees;
        }

        public void SetIsTourEchange(bool isTourEchange, int nbrTuilesEchangees = 0)
        {
            this.isTourEchange = isTourEchange;
            this.nbrTuilesEchangees = nbrTuilesEchangees;
        }

        public int GetNbrTuilesEchangees()
        {
            return nbrTuilesEchangees;
        }

        public int GetPoints()
        {
            return points;
        }

        public void SetPoints(int points)
        {
            this.points = points;
        }

        public void AnnulerPlacements()
        {
            casesModifiees.Clear();
            tuilesUtilisees.Clear();
            grille.ViderTuiles();

            grille.DecalageX = 0; grille.DecalageY = 0;

            Case[,] tableauCases = new Case[initialX, initialY];
            for (int largeur = 0; largeur < initialX; largeur++)
            {
                for (int hauteur = 0; hauteur < initialY; hauteur++)
                {
                    tableauCases[largeur, hauteur] = new Case(largeur, hauteur);
                }
            }
            grille.SetCases(tableauCases);
        }

        public void PlacerTuile(Tuile tuile, int x, int y)
        {
            grille.PlacerTuile(tuile, x, y);
            casesModifiees.Add(grille.GetCase(x, y));
            tuilesUtilisees.Add(tuile);
        }

        public Tuile GetTuile(int x, int y)
        {
            return this.grille.GetTuile(x, y);
        }

        public Grille GetGrille()
        {
            return this.grille;
        }

        public List<Case> GetCasesUtilisees()
        {
            return casesModifiees;
        }

        public List<Tuile> GetTuilesJoueur()
        {
            List<Tuile> resultat = new List<Tuile>();
            foreach (Tuile tuile in joueur.GetTuiles())
            {
                resultat.Add(tuile);
            }
            foreach (Tuile tuile in this.tuilesUtilisees)
            {
                resultat.Remove(tuile);
            }
            return resultat;
        }
    }
}
