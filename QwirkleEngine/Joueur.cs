﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Joueur
    {
        private string nom;
        private int points;
        private ReserveTuilesJoueur tuiles = new ReserveTuilesJoueur();
        public int PremierTour_nbrTuilesCaractereCommun { get; set; }

        public Joueur(string nom)
        {
            this.nom = nom;
            this.points = 0;
        }

        public ReserveTuilesJoueur GetReserve()
        {
            return tuiles;
        }

        public int GetPoints()
        {
            return this.points;
        }

        public string GetNom()
        {
            return this.nom;
        }

        public void AjouterPoints(int nbrPoints)
        {
            this.points += nbrPoints;
        }

        public void AjouterTuile(Tuile tuileRandom)
        {
            this.tuiles.AjouterTuile(tuileRandom);
        }

        public List<Tuile> GetTuiles()
        {
            return tuiles.GetTuiles();
        }
    }
}
