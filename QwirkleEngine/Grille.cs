﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Grille
    {
        private Case[,] cases;
        public int DecalageX { get; set; }
        public int DecalageY { get; set; }

        public Grille()
        {
            cases = new Case[3, 3];
            InitTableauCase(cases);
            DecalageX = DecalageY = 0;
        }

        public Tuile GetTuile(int x, int y)
        {
            return this.cases[x,y].GetTuile();
        }

        public void CopierCases(Case[,] source, Case[,] destination, int decalerX = 0, int decalerY = 0)
        {
            for (int largeur = 0; largeur < source.GetLength(0); largeur++)
            {
                for (int hauteur = 0; hauteur < source.GetLength(1); hauteur++)
                {
                    destination[largeur + decalerX, hauteur + decalerY] = source[largeur, hauteur];
                }
            }
        }

        private void InitTableauCase(Case[,] cases)
        {
            for (int x = 0; x < cases.GetLength(0); x++)
            {
                for (int y = 0; y < cases.GetLength(1); y++)
                {
                    cases[x, y] = new Case(x, y);
                }
            }
        }
        private void AgrandirGrilleGauche()
        {
            Case[,] casesNouveau = new Case[cases.GetLength(0) + 1, cases.GetLength(1)];
            InitTableauCase(casesNouveau);
            CopierCases(cases, casesNouveau, 1, 0);
            DecalageX++;
            cases = casesNouveau;
        }

        private void AgrandirGrilleDroite()
        {
            Case[,] casesNouveau = new Case[cases.GetLength(0) + 1, cases.GetLength(1)];
            InitTableauCase(casesNouveau);
            CopierCases(cases, casesNouveau);
            cases = casesNouveau;
        }

        private void AgrandirGrilleHaut()
        {
            Case[,] casesNouveau = new Case[cases.GetLength(0), cases.GetLength(1) + 1];
            InitTableauCase(casesNouveau);
            CopierCases(cases, casesNouveau);
            cases = casesNouveau;
        }

        private void AgrandirGrilleBas()
        {
            Case[,] casesNouveau = new Case[cases.GetLength(0), cases.GetLength(1) + 1];
            InitTableauCase(casesNouveau);
            CopierCases(cases, casesNouveau, 0, 1);
            DecalageY++;
            cases = casesNouveau;
        }

        public void PlacerTuile(Tuile tuile, int x, int y)
        {
            if (x == 0)
            {
                AgrandirGrilleGauche();
                x++;
            }
            if (y == 0)
            {
                AgrandirGrilleBas();
                y++;
            }
            if (x == cases.GetLength(0) - 1)
            {
                AgrandirGrilleDroite();
            }
            if (y == cases.GetLength(1) - 1)
            {
                AgrandirGrilleHaut();
            }

            cases[x, y].SetTuile(tuile);
        }

        public void ViderTuiles()
        {
            for (int indexColonne = 0; indexColonne < cases.GetLength(0); indexColonne++)
            {
                for (int indexLigne = 0; indexLigne < cases.GetLength(1); indexLigne++)
                {
                    cases[indexColonne, indexLigne].EnleverTuile();
                }
            }
        }

        public Case GetCase(int x, int y)
        {
            return cases[x, y];
        }

        public Case[,] GetCases()
        {
            return cases;
        }

        public void SetCases(Case[,] tableauCases)
        {
            this.cases = tableauCases;
        }

        public int GetLargeur()
        {
            return cases.GetLength(0);
        }

        public int GetHauteur()
        {
            return cases.GetLength(1);
        }
    }
}
