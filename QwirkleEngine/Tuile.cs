﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Tuile
    {
        public enum Couleur
        {
            Jaune,
            Vert,
            Bleu,
            Violet,
            Orange,
            Rouge
        }

        public enum Forme
        {
            Carre,
            Trefle,
            Cercle,
            Losange,
            Etoile8,
            Etoile4
        }

        private Couleur couleur;
        private Forme forme;

        public Tuile(Couleur couleur, Forme forme)
        {
            this.couleur = couleur;
            this.forme = forme;
        }

        public Couleur GetCouleur()
        {
            return this.couleur;
        }

        public Forme GetForme()
        {
            return this.forme;
        }
    }
}
