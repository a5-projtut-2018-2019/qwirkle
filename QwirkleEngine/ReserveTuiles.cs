﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public abstract class ReserveTuiles
    {
        protected List<Tuile> stockTuiles = new List<Tuile>();

        public List<Tuile> GetTuiles()
        {
            return stockTuiles;
        }

        public int GetNbrTuilesRestantes()
        {
            return stockTuiles.Count;
        }

        public int GetNbrTuilesRestantesCouleur(Tuile.Couleur couleur)
        {
            int nbrTuilesCorrespondantes = 0;
            foreach (Tuile tuile in stockTuiles)
            {
                if (tuile.GetCouleur() == couleur)
                {
                    nbrTuilesCorrespondantes++;
                }
            }
            return nbrTuilesCorrespondantes;
        }

        public int GetNbrTuilesRestantesForme(Tuile.Forme forme)
        {
            int nbrTuilesCorrespondantes = 0;
            foreach (Tuile tuile in stockTuiles)
            {
                if (tuile.GetForme() == forme)
                {
                    nbrTuilesCorrespondantes++;
                }
            }
            return nbrTuilesCorrespondantes;
        }

        public void AjouterTuile(Tuile tuile)
        {
            this.stockTuiles.Add(tuile);
        }

        public void EnleverTuile(Tuile tuile)
        {
            this.stockTuiles.Remove(tuile);
        }
    }
}
