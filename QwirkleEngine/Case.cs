﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Case
    {
        private int x;
        private int y;
        private Tuile tuile;

        public Case(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int GetX()
        {
            return this.x;
        }

        public int GetY()
        {
            return this.y;
        }

        public Tuile GetTuile()
        {
            return tuile;
        }

        public void SetTuile(Tuile tuile)
        {
            this.tuile = tuile;
        }

        public void EnleverTuile()
        {
            this.tuile = null;
        }
    }
}
