﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class Partie
    {
        private List<Joueur> listeJoueurs = new List<Joueur>();
        private List<Tour> historiqueTours = new List<Tour>();
        private Grille grille = new Grille();
        private int nbrJoueurs;
        private ReserveTuilesPartie stockTuiles = new ReserveTuilesPartie();
        private int indexJoueurActuel;
        private Tour tourActuel;
        private bool estListeJoueursOrdonnee = false;

        public Partie(List<Joueur> listeJoueurs)
        {
            nbrJoueurs = listeJoueurs.Count;
            stockTuiles.InitReserve();
            this.listeJoueurs = listeJoueurs;
            foreach (Joueur joueur in listeJoueurs)
            {
                Utilitaires.RemplirReserveJoueur(joueur.GetReserve(), stockTuiles);
            }
            CalculerOrdreJoueurs();
            indexJoueurActuel = 0;
            tourActuel = new Tour(this.listeJoueurs[indexJoueurActuel], 3, 3);
        }

        public int GetIndexJoueurActuel()
        {
            return indexJoueurActuel;
        }

        public int GetNbrToursJoues()
        {
            return historiqueTours.Count;
        }

        public double GetPointsMoyensParTour()
        {
            double moyenne = 0;
            foreach (Tour tour in historiqueTours)
            {
                moyenne += tour.GetPoints();
            }
            moyenne /= historiqueTours.Count;
            return moyenne;
        }

        public int GetMaxPointsEnUnCoup()
        {
            int maxPoints = 0;
            foreach (Tour tour in historiqueTours)
            {
                if (tour.GetPoints() > maxPoints)
                {
                    maxPoints = tour.GetPoints();
                }
            }
            return maxPoints;
        }

        public int GetNbrTuilesEchangees()
        {
            int nbrTuilesEchangees = 0;
            foreach (Tour tour in historiqueTours)
            {
                nbrTuilesEchangees += tour.GetNbrTuilesEchangees();
            }
            return nbrTuilesEchangees;
        }

        public List<Joueur> GetListeJoueursOrdonnee()
        {
            if (!estListeJoueursOrdonnee)
            {
                this.CalculerOrdreJoueurs();
            }
            return listeJoueurs;
        }

        public Tour GetTourActuel()
        {
            return tourActuel;
        }

        public Grille GetGrille()
        {
            return grille;
        }

        private int MaxCaractereCommunCouleur(List<Tuile> tuiles)
        {
            int maxCaractereCommun = 0;
            foreach (Tuile tuileCommun in tuiles)
            {
                int compteur = 0;
                Tuile.Couleur couleur = tuileCommun.GetCouleur();
                foreach (Tuile tuile in tuiles)
                {
                    if (tuile.GetCouleur() == couleur)
                    {
                        compteur++;
                    }
                }
                maxCaractereCommun = Math.Max(maxCaractereCommun, compteur);
            }

            return maxCaractereCommun;
        }

        private int MaxCaractereCommunForme(List<Tuile> tuiles)
        {
            int maxCaractereCommun = 0;
            foreach (Tuile tuileCommun in tuiles)
            {
                int compteur = 0;
                Tuile.Forme forme = tuileCommun.GetForme();
                foreach (Tuile tuile in tuiles)
                {
                    if (tuile.GetForme() == forme)
                    {
                        compteur++;
                    }
                }
                maxCaractereCommun = Math.Max(maxCaractereCommun, compteur);
            }

            return maxCaractereCommun;
        }

        public void CalculerOrdreJoueurs()
        {
            foreach (Joueur joueur in listeJoueurs)
            {
                int maxCouleur, maxForme;
                maxCouleur = MaxCaractereCommunCouleur(joueur.GetTuiles());
                maxForme = MaxCaractereCommunForme(joueur.GetTuiles());
                joueur.PremierTour_nbrTuilesCaractereCommun = Math.Max(maxCouleur, maxForme);
            }

            List<Joueur> listeOrdonnee = new List<Joueur>();
            for (int iterationTri = 0; iterationTri < nbrJoueurs; iterationTri++)
            {
                int indexPremierJoueur = 0, nbrTuilesCaracCommun = 0;
                for (int indexJoueur = 0; indexJoueur < listeJoueurs.Count; indexJoueur++)
                {
                    if (listeJoueurs[indexJoueur].PremierTour_nbrTuilesCaractereCommun > nbrTuilesCaracCommun)
                    {
                        indexPremierJoueur = indexJoueur;
                        nbrTuilesCaracCommun = listeJoueurs[indexJoueur].PremierTour_nbrTuilesCaractereCommun;
                    }
                }
                listeOrdonnee.Add(listeJoueurs[indexPremierJoueur]);
                listeJoueurs.RemoveAt(indexPremierJoueur);
            }

            listeJoueurs = listeOrdonnee;
            estListeJoueursOrdonnee = true;
        }

        public bool EstTerminee()
        {
            bool estTerminee = true;

            foreach (Joueur joueur in listeJoueurs)
            {
                int compteurJoueursVides = 0;
                if (joueur.GetTuiles().Count == 0)
                {
                    compteurJoueursVides++;
                }
                if (compteurJoueursVides < 1)
                {
                    estTerminee = false;
                }
            }
            if (this.stockTuiles.GetTuiles().Count > 0)
            {
                estTerminee = false;
            }

            return estTerminee;
        }

        private Grille FusionnerGrilles(Grille grilleUn, Grille grilleDeux, int decalageUnX = 0, int decalageUnY = 0)
        {
            Grille grilleResultat = new Grille();
            Case[,] nouvellesCases = new Case[Math.Max(grilleUn.GetLargeur(), grilleDeux.GetLargeur()), Math.Max(grilleUn.GetHauteur(), grilleDeux.GetHauteur())];

            for (int largeur = 0; largeur < nouvellesCases.GetLength(0); largeur++)
            {
                for (int hauteur = 0; hauteur < nouvellesCases.GetLength(1); hauteur++)
                {
                    nouvellesCases[largeur, hauteur] = new Case(largeur, hauteur);
                }
            }

            for (int largeur = 0; largeur < grilleUn.GetLargeur(); largeur++)
            {
                for (int hauteur = 0; hauteur < grilleUn.GetHauteur(); hauteur++)
                {
                    if (nouvellesCases[largeur, hauteur].GetTuile() == null)
                    {
                        nouvellesCases[largeur + decalageUnX, hauteur + decalageUnY] = grilleUn.GetCases()[largeur, hauteur];
                    }
                }
            }

            for (int largeur = 0; largeur < grilleDeux.GetLargeur(); largeur++)
            {
                for (int hauteur = 0; hauteur < grilleDeux.GetHauteur(); hauteur++)
                {
                    if (nouvellesCases[largeur, hauteur].GetTuile() == null)
                    {
                        nouvellesCases[largeur, hauteur] = grilleDeux.GetCases()[largeur, hauteur];
                    }
                }
            }

            grilleResultat.SetCases(nouvellesCases);
            return grilleResultat;
        }

        public void CalculerPointsTour()
        {
            if (tourActuel.IsTourEchange())
            {
                tourActuel.SetPoints(0);
            }
            else
            {
                int points = 0;

                List<Case> casesUtilisees = tourActuel.GetCasesUtilisees();
                int typeAlignement = AlignementTuiles(casesUtilisees);
                
                switch (typeAlignement)
                {
                    case 1:
                        List<List<Tuile>> listeVerticales = new List<List<Tuile>>();
                        foreach(Case @case in casesUtilisees)
                        {
                            List<Tuile> ligneVerticale1 = ConstruireLigneVerticale(grille, @case.GetX(), @case.GetY());
                            ligneVerticale1.Add(@case.GetTuile());
                            listeVerticales.Add(ligneVerticale1);
                        }
                        foreach(List<Tuile> ligne in listeVerticales)
                        {
                            if (ligne.Count == 6)
                            {
                                points += 6;
                            }
                            if (ligne.Count > 1)
                            {
                                points += ligne.Count;
                            }
                        }
                        List<Tuile> horizontale2 = ConstruireLigneHorizontale(grille, casesUtilisees[0].GetX(), casesUtilisees[0].GetY());
                        horizontale2.Add(casesUtilisees[0].GetTuile());
                        points += horizontale2.Count;
                        
                        break;
                    case 2:
                        List<List<Tuile>> listeHorizontales = new List<List<Tuile>>();
                        foreach (Case @case in casesUtilisees)
                        {
                            List<Tuile> ligneHorizontale1 = ConstruireLigneHorizontale(grille, @case.GetX(), @case.GetY());
                            ligneHorizontale1.Add(@case.GetTuile());
                            listeHorizontales.Add(ligneHorizontale1);
                        }
                        foreach (List<Tuile> ligne in listeHorizontales)
                        {
                            if (ligne.Count == 6)
                            {
                                points += 6;
                            }
                            if (ligne.Count > 1)
                            {
                                points += ligne.Count;
                            }
                        }
                        List<Tuile> verticale2 = ConstruireLigneVerticale(grille, casesUtilisees[0].GetX(), casesUtilisees[0].GetY());
                        verticale2.Add(casesUtilisees[0].GetTuile());
                        points += verticale2.Count;

                        break;
                    case 3:
                        List<Tuile> verticale = ConstruireLigneVerticale(grille, casesUtilisees[0].GetX(), casesUtilisees[0].GetY());
                        verticale.Add(casesUtilisees[0].GetTuile());
                        List<Tuile> horizontale = ConstruireLigneHorizontale(grille, casesUtilisees[0].GetX(), casesUtilisees[0].GetY());
                        horizontale.Add(casesUtilisees[0].GetTuile());
                        if (horizontale.Count > 1)
                        {
                            points += horizontale.Count;
                        }
                        if (verticale.Count > 1)
                        {
                            points += verticale.Count;
                        }
                        if (horizontale.Count == 1 && verticale.Count == 1)
                        {
                            points += 1;
                        }
                        if (verticale.Count == 6 || horizontale.Count == 6)
                        {
                            points += 6;
                        }
                        
                        break;
                }

                if (EstTerminee())
                {
                    points += 6;
                }

                tourActuel.SetPoints(points);
            }
        }

        public void ValiderTour()
        {
            if (!tourActuel.IsTourEchange())
            {
                grille = FusionnerGrilles(grille, tourActuel.GetGrille(), tourActuel.GetGrille().DecalageX, tourActuel.GetGrille().DecalageY);
            }

            CalculerPointsTour();
            int points;
            points = tourActuel.GetPoints();
            listeJoueurs[indexJoueurActuel].AjouterPoints(points);

            foreach (Tuile tuile in tourActuel.GetTuilesUtilisees())
            {
                Utilitaires.EchangeEntreReserves(tourActuel.GetJoueur().GetReserve(), stockTuiles, tuile);
            }
            Utilitaires.RemplirReserveJoueur(tourActuel.GetJoueur().GetReserve(), stockTuiles);

            historiqueTours.Add(tourActuel);
            indexJoueurActuel++;
            if (indexJoueurActuel >= listeJoueurs.Count)
            {
                indexJoueurActuel = 0;
            }
            tourActuel = new Tour(listeJoueurs[indexJoueurActuel], grille.GetLargeur(), grille.GetHauteur());
        }

        public ReserveTuilesPartie GetStockTuiles()
        {
            return this.stockTuiles;
        }

        public List<Tour> GetHistoriqueTours()
        {
            return historiqueTours;
        }

        public List<Case> CasesPossibles(Tuile tuile)
        {
            Grille grilleTest = new Grille();
            grilleTest = FusionnerGrilles(grille, tourActuel.GetGrille(), tourActuel.GetGrille().DecalageX, tourActuel.GetGrille().DecalageY);
            List<Case> casesPossibles = new List<Case>();
            Case[,] cases = grilleTest.GetCases();

            for (int indexColonne = 0; indexColonne < cases.GetLength(0); indexColonne++)
            {
                for (int indexLigne = 0; indexLigne < cases.GetLength(1); indexLigne++)
                {
                    bool resultat = VerifierPlacerTuile(tuile, indexColonne, indexLigne);
                    if (resultat)
                    {
                        casesPossibles.Add(cases[indexColonne, indexLigne]);
                    }
                }
            }

            return casesPossibles;
        }

        private bool VerifierLigneCaracCommun(List<Tuile> ligne)
        {
            bool aCouleurCommune = true, aFormeCommune = true;
            Tuile.Couleur couleurCommune = ligne[0].GetCouleur();
            Tuile.Forme formeCommune = ligne[0].GetForme();

            foreach (Tuile tuile in ligne)
            {
                if (tuile.GetCouleur() != couleurCommune)
                {
                    aCouleurCommune = false;
                }
                if (tuile.GetForme() != formeCommune)
                {
                    aFormeCommune = false;
                }
            }

            return aCouleurCommune || aFormeCommune;
        }

        private bool VerifierDoublonLigne(List<Tuile> ligne)
        {
            bool aDoublon = false;

            foreach (Tuile tuile in ligne)
            {
                Tuile.Couleur couleur = tuile.GetCouleur();
                Tuile.Forme forme = tuile.GetForme();
                List<Tuile> ligneTraitement = new List<Tuile>();
                foreach (Tuile tuilecopie in ligne)
                {
                    ligneTraitement.Add(tuilecopie);
                }
                ligneTraitement.Remove(tuile);

                foreach (Tuile tuileTraitement in ligneTraitement)
                {
                    if ((tuileTraitement.GetCouleur() == couleur) && (tuileTraitement.GetForme() == forme))
                    {
                        aDoublon = true;
                    }
                }
            }

            return aDoublon;
        }

        private bool VerifierValiditeLigne(List<Tuile> ligne)
        {
            bool estValide = true;

            if (ligne.Count > 6)
            {
                estValide = false;
            }

            if (!VerifierLigneCaracCommun(ligne))
            {
                estValide = false;
            }

            if (VerifierDoublonLigne(ligne))
            {
                estValide = false;
            }

            return estValide;
        }

        private bool VerifierTuileNEstPasDansVide(List<Tuile> verticale, List<Tuile> horizontale)
        {
            bool nestPasDansVideVerticale = true;
            bool nestPasDansVideHorizontale = true;

            if (verticale.Count == 1 && historiqueTours.Count > 0)
            {
                nestPasDansVideVerticale = false;
            }

            if (horizontale.Count == 1 && historiqueTours.Count > 0)
            {
                nestPasDansVideHorizontale = false;
            }

            return nestPasDansVideHorizontale || nestPasDansVideVerticale;
        }

        private int AlignementTuiles(List<Case> cases) 
        {
            if (cases.Count == 1)
            {
                return 3;
            }
            else
            {
                bool alignementHorizontal = true, alignementVertical = true;

                int coordHorizontal = cases[0].GetY();
                foreach (Case @case in cases)
                {
                    if (@case.GetY() != coordHorizontal)
                    {
                        alignementHorizontal = false;
                    }
                }
                int coorVertical = cases[0].GetX();
                foreach (Case @case in cases)
                {
                    if (@case.GetX() != coorVertical)
                    {
                        alignementVertical = false;
                    }
                }

                if (alignementHorizontal)
                {
                    return 1;
                }
                else if (alignementVertical)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
        }

        private List<Tuile> ConstruireLigneHorizontale(Grille grille, int x, int y)
        {
            List<Tuile> horizontale = new List<Tuile>();

            if (x > 0)
            {
                int indexX = x - 1;
                while (grille.GetCase(indexX, y).GetTuile() != null)
                {
                    horizontale.Add(grille.GetCase(indexX, y).GetTuile());
                    indexX--;
                }
            }

            if (x < grille.GetLargeur() - 1)
            {
                int indexX = x + 1;
                while (grille.GetCase(indexX, y).GetTuile() != null)
                {
                    horizontale.Add(grille.GetCase(indexX, y).GetTuile());
                    indexX++;
                }
            }

            return horizontale;
        }

        private List<Tuile> ConstruireLigneVerticale(Grille grille, int x, int y)
        {
            List<Tuile> verticale = new List<Tuile>();

            if (y > 0)
            {
                int indexY = y - 1;
                while (grille.GetCase(x, indexY).GetTuile() != null)
                {
                    verticale.Add(grille.GetCase(x, indexY).GetTuile());
                    indexY--;
                }
            }

            if (y < grille.GetHauteur() - 1)
            {
                int indexY = y + 1;
                while (grille.GetCase(x, indexY).GetTuile() != null)
                {
                    verticale.Add(grille.GetCase(x, indexY).GetTuile());
                    indexY++;
                }
            }

            return verticale;
        }

        public bool VerifierPlacerTuile(Tuile tuile, int x, int y)
        {
            List<Tuile> horizontale = new List<Tuile>();
            List<Tuile> verticale = new List<Tuile>();
            Grille grilleTest = new Grille();
            grilleTest = FusionnerGrilles(grille, tourActuel.GetGrille(), tourActuel.GetGrille().DecalageX, tourActuel.GetGrille().DecalageY);
            List<List<Tuile>> voisinage = new List<List<Tuile>>();
            voisinage.Add(horizontale);
            voisinage.Add(verticale);
            bool estPlacementValide;

            horizontale = ConstruireLigneHorizontale(grilleTest, x, y);
            horizontale.Add(tuile);
            verticale = ConstruireLigneVerticale(grilleTest, x, y);
            verticale.Add(tuile);
            estPlacementValide = VerifierValiditeLigne(horizontale) && VerifierValiditeLigne(verticale);

            estPlacementValide = estPlacementValide && VerifierTuileNEstPasDansVide(verticale, horizontale);

            bool sontTuilesAlignees;
            List<Case> casesRempliesTourActuel = tourActuel.GetCasesUtilisees();
            List<Case> casesRemplies = new List<Case>();
            foreach (Case @case in casesRempliesTourActuel)
            {
                casesRemplies.Add(@case);
            }
            casesRemplies.Add(grilleTest.GetCase(x, y));
            if (AlignementTuiles(casesRemplies) > 0)
            {
                sontTuilesAlignees = true;
            }
            else
            {
                sontTuilesAlignees = false;
            }
                
            return estPlacementValide && sontTuilesAlignees;
        }

        public Grille GetGrillePartieEtTour()
        {
            Grille nouvelleGrille = FusionnerGrilles(grille, tourActuel.GetGrille(), tourActuel.GetGrille().DecalageX, tourActuel.GetGrille().DecalageY);
            return nouvelleGrille;
        }
    }
}
