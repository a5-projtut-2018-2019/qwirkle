﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleEngine
{
    public class ReserveTuilesPartie : ReserveTuiles
    {
        public ReserveTuilesPartie()
        {
            
        }

        public void InitReserve()
        {
            foreach (Tuile.Couleur color in Enum.GetValues(typeof(Tuile.Couleur)))
            {
                foreach (Tuile.Forme shape in Enum.GetValues(typeof(Tuile.Forme)))
                {
                    for (int iteration = 0; iteration < 3; iteration++)
                    {
                        Tuile tuile = new Tuile(color, shape);
                        this.stockTuiles.Add(tuile);
                    }
                }
            }
        }
    }
}
